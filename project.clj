(defproject link.szabo.mauricio/duck-repled "0.1.2"
  :description "Query interactions with REPLs with Pathom"
  :url "https://gitlab.com/clj-editors/duck-repled"
  :license {:name "MIT"
            :url "https://mit-license.org/"}
  :dependencies [[metosin/malli "0.13.0"]
                 [com.wsscode/pathom3 "2023.08.22-alpha"]
                 [funcool/promesa "11.0.678"]
                 [link.szabo.mauricio/orbit "2024-11-13.02.17"]
                 [org.clojure/clojure "1.11.1"]
                 [rewrite-clj "1.1.47"]]

  :source-paths ["src" "orbit/src"]

  :repl-options {:init-ns user}
  :aliases {"watch" ["run" "-m" "user/watch"]
            "shadow-release" ["run" "-m" "user/release"]}
  :profiles {:dev {:dependencies [[thheller/shadow-cljs "2.25.2"]
                                  [check "0.2.2-SNAPSHOT"]
                                  [borkdude/sci "0.2.5"]
                                  ; [com.wsscode/pathom-viz-connector "2022.02.14"]

                                  [compliment "0.6.0"]
                                  [org.rksm/suitable "0.6.1"]]

                   :source-paths ["dev" "test" "orbit/test"]}})
