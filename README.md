# Duck-REPL'ed
[![<ORG_NAME>](https://circleci.com/gh/mauricioszabo/duck-repled.svg?style=svg)](<LINK>)

![A Duck](doc/duck.svg)

A library that transforms you REPL interactions into EQL queries. This means
that it's easy to hook up a REPL eval-response, and get autocomplete, var
definition, and other niceties "for free".

It'll also be used, in the future, both on Chlorine and Clover projects

## What's with the name?

It's a pun with "duck typed" - the idea that if something flies like a duck,
quacks like a duck, it probably is a duck. That's the spirit of this project: if
you can hook a REPL, you'll get an experience that's indistinguishable from a
nREPL, or Language Server Protocol, or even static analysis. So, it's
"duck-typing for REPLs" in a way.

As the idea of the project is also to be extensible, it's possible that if you
hook an evaluation of another language, but somehow renders EDN, you have a
clojure-like experience in a language where this would not be possible!

It's also a pun with "Pathom" and "Patão" (the [incredible library from Wilker
Lúcio](https://github.com/wilkerlucio/pathom) and "duck", in Portuguese).

## Documentation

Duck-REPLed is a collection of resolvers for the Pathom library - if you know
Pathom, you know Duck-REPLed. Some small changes to the way Pathom works is by
adding a "seed" resolver to every query - basically, if you do a query with an
entity and a query (like `(eql {:some/thing 10} [:some/query])` for example) the
entity will be present in each sub-element - meaning that:

```clojure
(eql {:some/thing 10} [{:editor/contents [:some/thing]}])
;; Will Return:
{:editor/contents {:some/thing 10}}
```

To use this library, you first need to generate the eql function, which you can
do with `duck-repled.core/gen-eql`. With no parameters, it'll generate a
"default" eql, but it accepts a map containing `:resolvers` and `:plugins` - the
second is just a vector containing Pathom plug-ins, and the former is just, by
default, `duck-repled.core/original-resolvers` containing all resolvers that
Duck-REPLed implements. You probably don't want to _change_ and concatenate
resolvers by hand, and instead use `duck-repled.core/add-resolver` to generate a
resolver dynamically, or `compose-resolver` which makes a new resolver that
outputs the same output from a different resolver (or resolvers) and change the
resolver resolution so that it'll first call the original resolver (or
resolvers) and then return that input to the new, "composed" resolver so you can
"augment" an existing output with something custom-made (for examples, check
[the test file](test/duck_repled/core_test.cljc))

### Attributes

All atributes are defined in the [schema file](src/duck_repled/schemas.cljc),
but this is not sufficient - it's a good idea to understand the dependencies
between resolvers.

So, there are some different types of resolvers - the ones that return REPL
evaluations, and the ones that return text information. A good example of REPL
evaluations is, well, `:repl/result` which will evaluate the code in some REPL
and then return the result; but there are other examples, like `:var/meta`
(which returns a Clojure metadata from a var), for example. Text information is
basically reading text from some editor, or some file, and then extracting
important info about that file - for example, `:text/current-var` extracts the
_current variable_ from a Clojure file, and `:text/block` extracts the _current
block_ of something - for Clojure, it'll be the open parenthesis/brackets that
the cursor is positioned, up to the close parenthesis/brackets; for Ruby, it'll
be the current method chain, up to the point the cursor is - so, for example,
the code:

```ruby
Something
  .new(10)
  .to_s
  .upcase
```

If you position the cursor at the second line, the "block" will be
`Something.new(10)`. If you position at the third line, it'll include `.to_s`,
and so on. The details can be tricky, and this is not yet the complete, fully
reliable, solution for Ruby (or the language that will come later).

#### Config attributes:

These are attributes the user must provide so that Duck-REPLed works as expected. These are:

* `:editor/data` - must be provided as a resolver or entity for `:editor/contents` to work. It is defined by `:editor/data`, an attribute containing `:text`, `:range`, and `:file`, so `:editor/contents`
* `:config/eval-as` - must be either `:clj`, `:cljs`, `:prefer-clj` or `:prefer-cljs`. The last two relate to how `.cljc` files are interpreted - if they should be interpreted as Clojure (the former) or ClojureScript (the later) files
* `:config/project-paths` - an array of strings containing the current project paths. It is used to define classpath in some situations, or to resolve relative paths, or config paths.
* `:repl/evaluator` - The evaluator for the language. Must be an implementation of the `orbit.evaluation/REPL` protocol.

#### Text attributes

For most "get text" information, the answer will be `:text/contents` and `:text/range`. So I'll just refer to "text attrs" for simplicity. So, for things that depends on "text attrs" and return  "text attrs":

* `:text/top-block` - return the "top block" of the code, whatever that means for the language
* `:text/block` - return the inner "block" of the code
* `:text/current-var` - return the variable under the :text/range in the :text/contents
* `:text/selection` - return the contents of the selected text in `:text/selection`, and basically copies `:text/range` over - meaning that multiple applications of `:text/selection` **won't work**
* `:text/ns` - for Clojure, returns the current namespace (considering the range the cursor currently is)

Things that _return_ text attrs:

* `:editor/contents` - depends on `:editor/data`. The contents of the current text editor. Also returns `:file/filename` (if the editor is saved to a file), which is the current filename of the editor.
* `:file/contents` - depends on `:file/filename`, and returns the same `:file/filename` together with text attrs. Reads the file, and returns the contents of it. You can "patch" the result to simulate a specific selection on the contents, for example with `(eql ['(:file/contents {:patch {:text/range [[1 0] [1 1]]}})])` to "select" the first character of the second row.

Other attributes:

* `:file/path` - given a vector of strings will "join" these as a path to return a `:file/path`. It's basically used to normalize windows, linux, and WSL paths
* `:file/filename` - a string. Some resolvers return `:file/filename`, but it might be passed on the entity position to read a file contents (but usually it's better to pass a `:file/path` and then ask for `:file/contents`)
* `:file/exists?` - a boolean. Checks if the current file exists

#### Config attributes

#### REPL attributes
   :definition/row (m/schema int?)
   :definition/col (m/schema int?)
   :definition/filename (m/schema string?)
   :definition/contents (m/schema [:maybe contents])
   :definition/source (m/schema contents)

   :ex/function-name (m/schema string?)
   :ex/filename (m/schema string?)
   :ex/row (m/schema int?)

   :repl/kind (m/schema keyword?)
   :repl/aux? (m/schema boolean?)
   :repl/namespace (m/schema simple-symbol?)
   :repl/evaluator (m/schema any?)
   :repl/code (m/schema string?)
   :repl/result (m/schema [:map [:result any?]])
   :repl/error (m/schema [:map [:error any?]])
   :repl/template (m/schema any?)
   :repl/cljs-env (m/schema string?)
   :repl/cljs-eval (m/schema any?)

   :var/meta (m/schema any?)
   :var/fqn (m/schema qualified-symbol?)

   :completions/var (m/schema completion)
   :completions/keyword (m/schema completion)
   :completions/nrepl (m/schema completion)
   :completions/external (m/schema completion)
   :completions/all (m/schema completion)
