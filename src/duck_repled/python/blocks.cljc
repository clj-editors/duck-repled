(ns duck-repled.python.blocks
  (:require [duck-repled.tree-sitter :as ts]
            [com.wsscode.pathom3.connect.operation :as pco]
            [duck-repled.connect :as connect]
            [duck-repled.editor-helpers :as editor-helpers]
            [clojure.string :as str]
            [promesa.core :as p]
            ["path" :as path]))

(p/do!
 ts/initialized
 (p/let [my-path js/__dirname
         python (.. ts/Parser -Language (load (path/join my-path "python.wasm")))]
   (def python-language python)
   (def parser (new ts/Parser))
   (.setLanguage parser python-language)))

(defn- to-range [^js node]
  (let [start (.-startPosition node)
        end (.-endPosition node)]
    [[(.-row start) (.-column start)]
     [(.-row end) (.-column end)]]))

(defn- get-node-block [^js node]
  (let [row (.. node -endPosition -row)]
    (loop [last-node node
           curr-node node]
      (if-let [node-row (some-> curr-node .-endPosition .-row)]
        (if (and (= node-row row) (-> curr-node .-type #{"expression_statement"} not))
          (recur curr-node (.-parent curr-node))
          last-node)
        last-node))))

(pco/defresolver python-ts [{:keys [parsers]} {:text/keys [contents language]}]
  {::pco/output [:python/node]}
  (when (= :python language)
    (let [parsed (.parse parser contents)]
      (swap! parsers conj parsed)
      {:python/node (ts/root parsed)})))

(pco/defresolver block [{:keys [python/node text/range]}]
  {::pco/output [{:text/block [:text/contents :text/range]}]}

  (let [[[r-start c-start] end] range
        [r-end c-end] end
        captures (ts/captures node
                              ts/any-query
                              {:row r-start :column (if (zero? c-start) 1 (dec c-start))}
                              {:row r-end :column c-end})
        last-node (->> captures
                       last
                       .-node)
        parent-node (.-parent last-node)
        node-to-consider (cond
                           (-> parent-node .-type (= "call"))
                           parent-node

                           (-> last-node .-type (= "argument_list"))
                           parent-node

                           (-> parent-node .-type (= "argument_list"))
                           (.. last-node -parent -parent)

                           :else
                           last-node)
        block-node (get-node-block node-to-consider)]
    {:text/block
     {:text/contents (.-text block-node)
      :text/range (to-range block-node)}}))

(defn- find-root-node [^js node]
  (let [parent (-> node .-parent delay)]
    (cond
      (#{"function_definition" "class_definition"} (.-type node)) node
      @parent (recur @parent)
      :no-parent node)))

(defn- make-patch-code [^js method, ^js class]
  (let [method-name (-> method .-children second .-text)
        class-name (-> class .-children second .-text)]
    (str "\n" class-name "." method-name " = " method-name)))

(connect/defresolver top-block [{:keys [python/node text/range]}]
  {::pco/output [{:text/top-block [:text/contents :text/range :repl/watch-point]}]}
  (let [[[r-start c-start] end] range
        [r-end c-end] end
        captures (ts/captures node
                              ts/any-query
                              {:row r-start :column (if (zero? c-start) 0 (dec c-start))}
                              {:row r-end :column c-end})
        top-block-node (-> captures
                           last
                           .-node
                           (find-root-node))
        class-definition (when (-> top-block-node .-type (= "function_definition"))
                           (find-root-node (.-parent top-block-node)))
        is-class-def? (some-> class-definition .-type (= "class_definition"))
        additional-content (when is-class-def?
                             (make-patch-code top-block-node class-definition))]
    {:text/top-block (cond-> {:text/contents (str (.-text top-block-node) additional-content)
                              :text/range (to-range top-block-node)
                              :repl/watch-point ""}
                       (and class-definition (not is-class-def?)) (dissoc :repl/watch-point))}))

(connect/defresolver selection [{:keys [text/contents text/range text/language]}]
  {::pco/output [{:text/selection [:text/contents :text/range]}]
   ::pco/priority 10}
  (when (= :python language)
    (when-let [text (editor-helpers/text-in-range contents range)]
      (let [lines (.split text #"\n")
            biggest (->> lines
                         (map #(count (re-find #"^\s*" %)))
                         (apply min))]

        {:text/selection
         {:text/range range
          :text/contents (->> lines
                              (map #(str/replace % (re-pattern (str "^\\s{" biggest "}")) ""))
                              (str/join "\n"))}}))))

(def resolvers [python-ts block top-block selection])
