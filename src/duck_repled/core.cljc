(ns duck-repled.core
  (:require [com.wsscode.pathom3.connect.indexes :as indexes]
            [com.wsscode.pathom3.interface.async.eql :as eql]
            [com.wsscode.pathom3.connect.runner :as runner]
            [com.wsscode.pathom3.connect.planner :as pcp]
            [com.wsscode.pathom3.plugin :as plugin]
            [com.wsscode.pathom3.connect.built-in.plugins :as plugins]
            [duck-repled.schemas :as schemas]
            [duck-repled.editor-resolvers :as editor]
            [duck-repled.repl-resolvers :as repl]
            [duck-repled.definition-resolvers :as def]
            [duck-repled.autocomplete-resolvers :as auto]
            [duck-repled.clojure.text-resolvers :as clj]
            [promesa.core :as p]
            [com.wsscode.pathom3.connect.operation :as pco]
            [clojure.set :as set]

            ; Languages
            [duck-repled.ruby.blocks :as rb-blocks]
            [duck-repled.python.blocks :as py-blocks]
            [duck-repled.ruby.features :as rb-features]))


(def original-resolvers (vec (concat editor/resolvers
                                     repl/resolvers
                                     def/resolvers
                                     clj/resolvers
                                     auto/resolvers

                                     rb-blocks/resolvers
                                     py-blocks/resolvers
                                     rb-features/resolvers)))

(defn- gen-resolver-fun [fun outputs]
  (fn [_ input]
    (p/let [result (fun input)]
      (schemas/validate! (keys result)
                         result
                         (str "Invalid schema on custom resolver outputing " outputs)))))

(def CONNECT_PARSER? (and js/goog.DEBUG (not js/process.env.CI)))

(defn seed-resolver [seed]
  (pco/resolver `seed
                {::pco/input []
                 ::pco/output (-> seed keys vec)
                 ::pco/priority 99}
                (fn [_ _] seed)))

(defn gen-eql
  ([] (gen-eql {}))
  ([{:keys [resolvers plugin]
     :or {resolvers original-resolvers
          plugin identity}}]
   (let [plan-cache (atom {})
         state (atom {})
         env (-> resolvers
                 indexes/register
                 (plugin/register (plugins/attribute-errors-plugin))
                 (pcp/with-plan-cache plan-cache)
                 plugin)]

     (fn q
       ([query] (q {} query))
       ([seed query]
        (when js/goog.DEBUG
          (schemas/validate! (keys seed) seed))
        (p/let [parsers (atom [])
                res (eql/process (-> env
                                     (assoc :com.wsscode.pathom3.error/lenient-mode? true)
                                     (assoc :parsers parsers)
                                     (assoc :state state)
                                     (assoc :q q)
                                     (indexes/register (seed-resolver seed)))
                                 seed
                                 query)]
          (doseq [^js parser @parsers] (.delete parser))
          res))))))

(defn add-resolver
  ([config fun] (add-resolver original-resolvers config fun))
  ([resolvers {:keys [inputs outputs priority] :as config} fun]
   (when-let [errors (schemas/explain-add-resolver config)]
     (throw (ex-info "Input to add-resolver is invalid" {:errors errors})))

   (conj resolvers
         (pco/resolver (gensym "custom-resolver-")
                       {::pco/input inputs
                        ::pco/output outputs
                        ::pco/priority (or priority 50)}
                       (gen-resolver-fun fun outputs)))))

(defn- rename-resolve-out [resolve-out]
  (let [out-ns (namespace resolve-out)
        out-name (name resolve-out)]
    (keyword out-ns (str out-name "-rewrote"))))

(defn- rename-resolvers-that-output [resolvers outputs]
  (let [rewroted-map (zipmap outputs (map rename-resolve-out outputs))]
    (for [resolver resolvers
          :let [resolver-out (-> resolver :config ::pco/output)
                new-out (mapv #(cond-> % (rewroted-map %) rewroted-map)
                              resolver-out)
                fun (:resolve resolver)]]
      (if (= resolver-out new-out)
        resolver
        (pco/resolver (-> resolver :config ::pco/op-name (str "-renamed") symbol)
                      {::pco/input (-> resolver :config ::pco/input)
                       ::pco/output new-out
                       ::pco/priority (-> resolver :config (::pco/priority 0))}
                      (fn [ & args]
                        (p/let [res (apply fun args)]
                          (set/rename-keys res rewroted-map))))))))

(defn compose-resolver
  ([config fun] (compose-resolver original-resolvers config fun))
  ([resolvers {:keys [inputs outputs priority] :as config} fun]
   (when-let [errors (schemas/explain-add-resolver config)]
     (throw (ex-info "Input to add-resolver is invalid" {:errors errors})))

   (let [renamed-resolvers (rename-resolvers-that-output resolvers outputs)
         renamed (map rename-resolve-out outputs)
         inputs (into inputs renamed)
         fun (fn [input]
               (-> input
                   (set/rename-keys (zipmap renamed outputs))
                   fun))]
     (-> renamed-resolvers
         vec
         (conj (pco/resolver (gensym "custom-resolver-")
                             {::pco/input inputs
                              ::pco/output outputs
                              ::pco/priority (or priority 50)}
                             (gen-resolver-fun fun outputs)))))))

; (defn other-path [{::pcp/keys [graph] :as env} or-node node-ids]
;   (let [nodes (for [id node-ids
;                     parent (pcp/node-successors graph id)
;                     :let [expects (-> or-node :com.wsscode.pathom3.connect.planner/expects keys)
;                           config (pcp/node-with-resolver-config graph env parent)
;                           provides (:com.wsscode.pathom3.connect.operation/provides config)]
;                     :when (and provides (every? provides expects))]
;                 [id config])
;         [id] (apply max-key #(-> % second (::pco/priority 0)) nodes)]
;     (or id (first node-ids))))
