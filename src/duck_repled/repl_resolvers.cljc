(ns duck-repled.repl-resolvers
  (:require [clojure.string :as str]
            [duck-repled.connect :as connect]
            [com.wsscode.pathom3.connect.operation :as pco]
            [orbit.evaluation :as eval]
            [orbit.shadow.evaluator :as shadow]
            [duck-repled.template :refer [template]]
            [duck-repled.editor-helpers :as helpers]
            [clojure.walk :as walk]
            [promesa.core :as p]))

;; FIXME: This should be in Orbit
(defn- get-shadow-build-id [evaluator]
  (let [state @(:state evaluator)
        client-id (:current-id state)]
    (get-in state [:id->build client-id])))

(connect/defresolver shadow-repl-env [{:keys [repl/evaluator]}]
  {::pco/output [:repl/cljs-env :repl/cljs-eval]}
  (when-let [env (shadow/get-env-var-code evaluator)]
    {:repl/cljs-env env
     :repl/cljs-eval
     `(fn [ns# code#]
        (let [res#
              (shadow.cljs.devtools.api/cljs-eval
               ~(get-shadow-build-id evaluator) code# {:ns (symbol ns#)})]
          (if-let [results# (:results res#)]
            {:value (clojure.edn/read-string (last results#))}
            {:error res#})))}))

(connect/defresolver repl-eval [env inputs]
  {::pco/input [:repl/evaluator :text/contents
                (pco/? :repl/aux?) (pco/? :repl/kind)
                (pco/? :file/filename) (pco/? :repl/namespace) (pco/? :text/range)]
   ::pco/output [:repl/result :repl/error]}

  (p/let [{:repl/keys [evaluator namespace aux? kind]
           :text/keys [contents range]
           :file/keys [filename]}
          inputs

          params (pco/params env)
          option-kind (when kind (keyword (name kind) (if aux? "aux" "eval")))
          opts (cond-> (dissoc params :repl/template)
                       option-kind (assoc-in [:options :kind] option-kind)
                       namespace (assoc :namespace namespace)
                       filename (assoc :filename filename)
                       range (-> (update :row #(or % (-> range first first)))
                                 (update :col #(or % (-> range first second)))))
          code (if-let [t (:repl/template params)]
                 (template t {:repl/code (symbol contents)})
                 contents)
          result (eval/evaluate evaluator code opts)]
    (if (:error result)
      {:repl/error result}
      {:repl/result result})))

(defn- extract-right-var [current-var contents]
  (let [contents (or (:text/contents current-var) contents)
        [_ var] (helpers/current-var (str contents) [0 0])]
    (when (and var (= var contents))
      contents)))

(connect/defresolver fqn-var
  [{:keys [repl/namespace text/current-var text/contents repl/evaluator
           repl/kind repl/aux?]}]
  {::pco/input [:repl/namespace :repl/evaluator
                (pco/? :repl/kind) (pco/? :repl/aux?)
                (pco/? :text/current-var) (pco/? :text/contents)]
   ::pco/output [:var/fqn]}

  (when-let [contents (extract-right-var current-var contents)]
    (p/let [option-kind (when kind (keyword (name kind) (if aux? "aux" "eval")))
            {:keys [result]} (eval/evaluate evaluator
                                            (str "`" contents)
                                            {:namespace namespace
                                             :options {:kind option-kind}})]
      {:var/fqn result})))

(connect/defresolver clj-meta-for-var
  [{:keys [repl/namespace repl/evaluator repl/kind text/current-var text/language]}]
  {::pco/output [:var/meta] ::pco/priority 1}

  (when (and (= :clojure language) (not= :cljs kind))
    (p/let [option-kind (keyword (name kind) "aux")
            code (template `(meta ::current-var)
                           {::current-var (->> current-var
                                               :text/contents
                                               (str "#'")
                                               symbol)})
            {:keys [result error]} (eval/evaluate evaluator
                                                  code
                                                  {:namespace namespace
                                                   :options {:kind option-kind}})]
      (cond
        result {:var/meta result}))))

(connect/defresolver cljs-meta-for-var
  [{:keys [repl/evaluator repl/kind repl/cljs-env text/current-var text/language]
    ns-symbol :repl/namespace}]
  {::pco/output [:var/meta] ::pco/priority 1}

  (when (and (= :clojure language) (= :cljs kind))
    (p/let [fqn (eval/evaluate evaluator
                               (str "`" (:text/contents current-var))
                               {:namespace ns-symbol})]
      (when-let [fqn (:result fqn)]
        (p/let [ns-var-part (-> fqn namespace symbol)
                var-part (-> fqn name symbol)
                code (template `(let [the-ns# (-> ::cljs-env
                                                  :cljs.analyzer/namespaces
                                                  (get '::ns-var-part))]
                                  (or (get-in the-ns# [:macros '::var-part])
                                      (get-in the-ns# [:defs '::var-part])))
                               {::cljs-env (symbol cljs-env)
                                ::ns-var-part ns-var-part
                                ::var-part var-part})
                result (eval/evaluate evaluator code {:options {:kind :clj/aux}})]
          (when-let [meta (:result result)]
            {:var/meta meta}))))))

; TODO: Somehow, test this
(pco/defresolver spec-for-var [{:keys [var/fqn repl/evaluator]}]
  {::pco/output [:var/spec]}

  (p/let [res (eval/evaluate evaluator "(require 'clojure.spec.alpha)" {})]
    (when-not (:error res)
      (p/let [{:keys [result]}
              (eval/evaluate
               evaluator
               (template `(let [s# (clojure.spec.alpha/get-spec ' ::fqn)
                                fun# #(some->> (% s) clojure.spec.alpha/describe)]
                            (when s#
                              (->> [:args :ret :fn]
                                   (map (juxt identity fun#))
                                   (filter second)
                                   (into {}))))
                         {::fqn fqn})
               {})]
        (when result {:var/spec result})))))

#_
(pco/defresolver doc-for-var [{:var/keys [fqn meta spec] :text/keys [language]}]
  {::pco/input [:var/fqn :var/meta (pco/? :var/spec) :text/language]
   ::pco/output [:var/doc]}

  (when (= :clojure language)
    {:var/doc
     (str "-------------------------\n"
          fqn "\n"
          (:arglists meta) "\n  "
          (:doc meta)
          (when (map? spec)
            (cond-> "\nSpec\n"
              (:args spec) (str "  args: " (pr-str (:args spec)) "\n")
              (:ret spec) (str "  ret: " (pr-str (:ret spec)) "\n")
              (:fn spec) (str "  fn: " (pr-str (:fn spec))))))}))

(def resolvers [shadow-repl-env repl-eval fqn-var
                clj-meta-for-var cljs-meta-for-var
                spec-for-var #_doc-for-var])
