(ns duck-repled.ruby.features
  (:require [duck-repled.tree-sitter :as ts]
            [com.wsscode.pathom3.connect.operation :as pco]
            [orbit.evaluation :as eval]
            [duck-repled.connect :as connect]
            [clojure.string :as str]
            [promesa.core :as p]
            [clojure.edn :as edn]
            ["fs" :as fs]))

(connect/defresolver complete-code [{:keys [ruby/dissected]}]
  {::pco/output [:completions/code]}
  (let [method-code (str "binding.local_variables.map { |m| ['local_var', m.to_s] } + "
                         "__self__.public_methods.map { |m| ['pub_method', m.to_s] } + "
                         "__self__.private_methods.map { |m| ['priv_method', m.to_s] } + "
                         "__self__.protected_methods.map { |m| ['prot_method', m.to_s] }")
        reg (apply str (interpose ".*" (:identifier dissected)))
        norm-symbol (str "Symbol.all_symbols.lazy.select { |x| x.inspect =~ /" reg "/ }"
                         ".select { |x| x.inspect !~ /\\\\x/ }")
        code (case (:type dissected)
               "constant" "::Object.constants.map { |i| ['constant', i.to_s] }"
               "instance_variable" "instance_variables.map { |i| ['instance_var', i.to_s] }"
               "class_variable" "class_variables.map { |i| ['class_var', i.to_s] }"
               "identifier" (str (str/replace method-code #"__self__\." "")
                                 " + " norm-symbol ".map { |x| ['symbol', x.to_s + ':'] }.to_a")
               "scope_resolution" (str (:callee dissected) ".constants.map { |i| ['constant', i.to_s] }")
               "call" (if (-> dissected :sep (= "::"))
                        (str (:callee dissected) ".constants.map { |i| ['constant', i.to_s] }")
                        (str/replace method-code #"__self__" (:callee dissected)))
               "simple_symbol" (str norm-symbol ".map { |x| ['symbol', ':' + x.to_s] }.to_a"))]
    {:completions/code code}))

(connect/defresolver complete-result [{:keys [:completions/code :repl/evaluator
                                              :text/language
                                              :file/filename :text/range]}]
  {::pco/input [:completions/code :repl/evaluator :text/language :text/range
                (pco/? :file/filename)]
   ::pco/output [:completions/all]}
  (when (= :ruby language)
    (p/let [params (cond-> {:row (ffirst range) :plain true :no-wrap true}
                     filename (assoc :filename filename))
            result (eval/evaluate evaluator code params)
            result (edn/read-string (pr-str result))]
      {:completions/all (mapv (fn [[kind var]]
                                {:text/contents var
                                 :completion/type (case kind
                                                    "local_var" :local
                                                    "pub_method" :method/public
                                                    "priv_method" :method/private
                                                    "prot_method" :method/protected
                                                    "instance_var" :property
                                                    "class_var" :property
                                                    "constant" :constant
                                                    "symbol" :keyword
                                                    :other)})
                              result)})))

(connect/defresolver ruby-find-definition [{:keys [file/filename text/range ruby/dissected repl/evaluator]}]
  {::pco/output [:definition/filename :definition/row]}
  (p/let [callee (:callee dissected "self")
          callee-class (str callee
                            (if (-> dissected :callee-type #{"scope_resolution" "constant"})
                              ".singleton_class"
                              ".class"))
          assign-is-the-expression? (delay (= (:assign/left-side dissected)
                                              (str (:callee dissected)
                                                   (:sep dissected)
                                                   (:identifier dissected))))
          identifier (str (:identifier dissected)
                          (when (-> dissected :type (= "call") (and @assign-is-the-expression?))
                            "="))
          code (case (:type dissected)
                 "call" (str callee-class ".__lazuli_source_location(:" identifier ")")
                 "identifier" (str callee-class ".__lazuli_source_location(:" identifier ")")
                 "constant" (str "Object.const_source_location(" (:identifier dissected) ".name)")
                 "scope_resolution" (str callee ".const_source_location(" (pr-str (:identifier dissected)) ")")
                 nil)]
    (when code
      (p/let [[file row] (eval/evaluate evaluator
                                        {:code code
                                         :file filename
                                         :line (ffirst range)}
                                        {:plain true :options {:op "eval"}})]
        (when (fs/existsSync file)
          {:definition/filename file
            :definition/row (dec row)})))))

(def resolvers [complete-code complete-result ruby-find-definition])
