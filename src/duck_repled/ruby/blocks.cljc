(ns duck-repled.ruby.blocks
  (:require [duck-repled.tree-sitter :as ts]
            [com.wsscode.pathom3.connect.operation :as pco]
            [clojure.string :as str]
            [promesa.core :as p]))

(defn- to-range [^js node]
  (let [start (.-startPosition node)
        end (.-endPosition node)]
    [[(.-row start) (.-column start)]
     [(.-row end) (.-column end)]]))

(defn- dissect-node [^js node]
  (let [myself (.-text node)
        my-type (.-type node)
        parent (.-parent node)
        parent-type (.-type parent)
        [callee sep caller params?] (.-children parent)
        my-children (delay (mapv (fn [^js node]
                                   [(.-text node)
                                    (.-type node)])
                                 (.-children node)))
        callee-txt (.-text callee)
        assign (fn [element ^js val]
                 (if (#{"operator_assignment" "assignment"} (.-type val))
                   (let [[l a] (.-children val)]
                     (assoc element
                            :assign/expression (str (.-text l) " " (.-text a))
                            :assign/left-side (.-text l)))
                   element))
        params (fn [params]
                 (when-let [params (some-> params .-text)]
                   (if (str/starts-with? params "(")
                     params
                     (str " " params))))
        flat-scope? (and (= "scope_resolution" parent-type)
                         (nil? caller))]

    (cond
      (#{"call" "scope_resolution"} my-type)
      (assign {:identifier ""
               :type my-type
               :sep (-> @my-children second first)
               :callee (ffirst @my-children)
               :callee-type (ffirst @my-children)}
              parent)

      flat-scope?
      {:identifier (str "::" myself)
       :type my-type}

      (= "scope_resolution" parent-type)
      (assign {:identifier (.-text caller)
               :type "scope_resolution"
               :sep (.-text sep)
               :callee callee-txt
               :callee-type (.-type callee)}
              (.-parent parent))

      (or (not= "call" parent-type) (= myself callee-txt))
      (assign {:identifier myself
               :type my-type
               :range (to-range node)}
              parent)

      caller
      (assign {:identifier (.-text caller)
               :type "call"
               :params (params params?)
               :sep (.-text sep)
               :callee callee-txt
               :callee-type (.-type callee)}
              (.-parent parent))

      callee
      (assign {:identifier (.-text callee)
               :type "call"
               :params (params sep)}
              (.-parent parent))

      :else
      {:identifier myself
       :type my-type
       :range (to-range node)})))

(defn- contextualize-method [parents ^js capture]
  (let [parents-str (reduce (fn [acc capture]
                              (let [[kind name] (.. capture -node -children)]
                                (conj acc (str (.-text kind) " " (.-text name)))))
                            []
                            parents)
        last-node (.-node capture)
        start-pos (.-startPosition last-node)
        end-pos (.-endPosition last-node)
        num-parents (count parents-str)
        final-txt (str (str/join ";" parents-str)
                       (when (seq parents-str) ";")
                       (-> capture .-node .-text str/trim-newline)
                       (when (seq parents-str) ";")
                       (str/join ";" (repeat num-parents "end")))]
    [final-txt [[(.-row start-pos) 0]
                [(.-row end-pos) (-> final-txt str/split-lines last count)]]]))

(pco/defresolver ruby-ts [{:keys [parsers]} {:text/keys [contents language]}]
  {::pco/output [:ruby/node]}
  (when (= :ruby language)
    (let [parsed (ts/parse contents)]
      (swap! parsers conj parsed)
      {:ruby/node (ts/root parsed)})))

(pco/defresolver ruby-dissected [{:keys [ruby/node text/range]}]
  {::pco/output [:ruby/dissected]}
  (let [[[r-start c-start] end] range
        [r-end c-end] end
        captures (ts/captures node
                              ts/any-query
                              {:row r-start :column (dec c-start)}
                              {:row r-end :column c-end})
        last-node (->> captures
                       last
                       .-node)
        parent (.-parent last-node)
        dissected (if (-> parent .-type (= "right_assignment_list"))
                    {:type "literal"
                     :identifier (.. parent -parent -text)
                     :start (.. ^js parent -parent getStartPosition)}
                    (dissect-node last-node))]
    {:ruby/dissected dissected}))

(pco/defresolver top-block [{:ruby/keys [node] :text/keys [contents range]}]
  {::pco/output [{:text/top-block [:text/contents :text/range :repl/watch-point]}]}
  (p/let [s-row (ffirst range)
          e-row (-> contents str/split-lines (nth s-row) count dec)
          captures-res (ts/captures node ts/parent-query
                                    {:row s-row :column e-row}
                                    {:row s-row :column e-row})
          [parents [capture]] (split-with #(= "parent" (.-name %)) captures-res)
          [modified-block new-range] (cond
                                       capture (contextualize-method parents capture)
                                       (seq parents) (contextualize-method (butlast parents) (last parents))
                                       :not-inside-module
                                       (when-let [^js node (-> node
                                                               (ts/captures ts/any-query
                                                                            {:row s-row :column e-row}
                                                                            {:row s-row :column e-row})
                                                               second
                                                               .-node)]
                                         (let [start-pos (.-startPosition node)
                                               end-pos (.-endPosition node)]
                                           [(.-text node)
                                            [[(.-row start-pos) (.-column start-pos)]
                                             [(.-row end-pos) (.-column end-pos)]]])))]
    (when modified-block
      {:text/top-block {:text/contents modified-block
                        :text/range new-range
                        :repl/watch-point ""}})))

(defn- get-node-block [^js node]
  (let [row (.. node -endPosition -row)]
    (loop [last-node node
           curr-node node]
      (if-let [node-row (some-> curr-node .-endPosition .-row)]
        (if (and (= node-row row) (-> curr-node .-type #{"program" "body_statement"} not))
          (recur curr-node (.-parent curr-node))
          last-node)
        last-node))))

(pco/defresolver block [{:keys [ruby/node text/range]}]
  {::pco/output [{:text/block [:text/contents :text/range]}]}

  (let [[[r-start c-start] end] range
        [r-end c-end] end
        captures (ts/captures node
                              ts/any-query
                              {:row r-start :column (if (zero? c-start) 1 (dec c-start))}
                              {:row r-end :column c-end})
        last-node (->> captures
                       last
                       .-node)
        node-to-consider (cond
                           (-> last-node .-type (= "argument_list"))
                           (.-parent last-node)

                           (-> last-node .-parent .-type (= "argument_list"))
                           (.. last-node -parent -parent)

                           :else
                           last-node)
        block-node (get-node-block node-to-consider)]
    {:text/block
     {:text/contents (.-text block-node)
      :text/range (to-range block-node)}}))

(def resolvers [ruby-ts ruby-dissected top-block block])
