(ns duck-repled.definition-resolvers
  (:require [clojure.string :as str]
            [duck-repled.connect :as connect]
            [com.wsscode.pathom3.connect.operation :as pco]
            [orbit.evaluation :as eval]
            [duck-repled.template :refer [template]]
            [duck-repled.editor-helpers :as helpers]
            [promesa.core :as p]
            #?(:cljs ["fs" :refer [existsSync statSync]])
            #?(:cljs ["path" :refer [join]])
            #?(:cljs ["os" :refer [platform]]))
  #?(:clj (:import [java.io File]
                   [java.nio.file FileSystems])))

(defn- join-paths [paths]
  #?(:cljs (apply join paths)
     :clj (if (empty? paths)
            "."
            (let [[fst & rst] paths]
              (-> (FileSystems/getDefault)
                  (.getPath fst (into-array rst))
                  str)))))

(defn- file-exists? [path]
  #?(:clj (.isFile (File. path))
     :cljs (and (existsSync path)
                (.isFile (statSync path)))))

(connect/defresolver join-paths-resolver [{:keys [file/path]}]
  {:file/filename (join-paths path)})

(connect/defresolver file-exists-resolver [{:keys [:file/filename]}]
  {:file/exists? (file-exists? filename)})

; (connect/defresolver position-resolver [{:keys [:var/meta]}]
;   {::pco/output [:definition/row :definition/col]}
;
;   (when-let [line (:line meta)]
;     (cond-> {:definition/row (-> meta :line dec)}
;             (:column meta) (assoc :definition/col (-> meta :column dec)))))

(defn- norm-result [file-name]
  (let [os #?(:clj (System/getProperty "os.name")
              :cljs (platform))]
    (cond-> file-name
            (and (re-find #"(?i)^win" os))
            (str/replace-first #"^/" ""))))

#_
(connect/defresolver seed-filename [input]
  {::pco/input [:var/meta (pco/? :ex/filename)]
   ::pco/output [:file/filename]
   ::pco/priority 30}
  (prn :seed-filename input)
  (when-let [file (or (-> input :ex/filename) (-> input :var/meta :file))]
    {:file/filename file}))

; (connect/defresolver existing-row-col [inputs]
;   {::pco/input [(pco/? :ex/row) (pco/? :ex/col)]
;    ::pco/output [:definition/row :definition/col]
;    ::pco/priority 30}
;   (cond-> {}
;     (contains? inputs :ex/row) (assoc :definition/row (-> (:ex/row inputs) (or 1) dec))
;     (contains? inputs :ex/col) (assoc :definition/col (-> (:ex/col inputs) (or 1) dec))))

; (connect/defresolver existing-filename [{:keys [:file/filename :file/exists?]}]
;   {::pco/input [:file/filename :file/exists?]
;    ::pco/output [:definition/filename :definition/contents]
;    ::pco/priority 30}
;   (when exists?
;     {:definition/filename filename :definition/contents nil}))

(defn- read-jar [repl jar-file-name]
  (let [[jar path] (str/split jar-file-name #"!/" 2)
        jar (clojure.string/replace-first jar #"file:" "")
        t `(let [jar-file# (java.util.jar.JarFile. ::jar)
                 ba# (java.io.ByteArrayOutputStream.)
                 is# (.getInputStream jar-file# (.getJarEntry jar-file# ::path))]
             (clojure.java.io/copy is# ba#)
             (java.lang.String. (.toByteArray ba#)))
        code (template t {::jar jar ::path path})]
    (eval/evaluate repl code {:options {:kind :clj/aux}})))

; (connect/defresolver file-from-clr [{:keys [:repl/evaluator :var/meta :repl/kind]}]
;   {::pco/output [:definition/filename]}
;   #_
;   (when (= :cljr kind)
;     (p/let [code (template `(some-> ::file
;                                     clojure.lang.RT/FindFile
;                                     str)
;                            {::file (:file meta)})
;             {:keys [result]} (repl/eval evaluator code)]
;       (when result
;         {:definition/filename (norm-result result)}))))

(defn- from-clj [file row col evaluator]
  (p/let [code (template `(do
                            (require 'clojure.java.io)
                            (some->> :file
                                     (.getResource (clojure.lang.RT/baseLoader))
                                     .getPath))
                         {:file file})
          {:keys [result]} (eval/evaluate evaluator code {:options {:kind :clj/aux}})
          filename (or (norm-result result) file)]
    (if (re-find #"\.jar!/" filename)
      (p/let [{:keys [result]} (read-jar evaluator filename)
              pos [row col]]
        {:definition/filename filename
         :definition/contents {:text/contents result
                               :text/range [pos pos]}})
      {:definition/filename filename})))

(connect/defresolver from-meta [{:keys [var/meta repl/evaluator]}]
  {::pco/input [:var/meta (pco/? :repl/evaluator)]
   ::pco/output [:definition/filename :definition/row :definition/col :definition/contents]}
  (let [{:keys [file line column]} meta
        row (dec line)
        col (some-> column dec)
        add-pos #(cond-> (assoc % :definition/row row)
                   column (assoc :definition/col col))]
    (cond
      (file-exists? file)
      (add-pos {:definition/filename file})

      evaluator
      (p/then (from-clj file row col evaluator) add-pos))))

(defn- clj-stack [function-name filename evaluator]
  (p/let [ns-name (-> function-name (str/split #"/") first)
          code (template `(let [n# (find-ns '::namespace-sym)]
                            (->> n#
                                 ns-interns
                                 (some (fn [[_# res#]]
                                         (let [meta# (meta res#)
                                               file# (-> meta# :file str)]
                                           (and (clojure.string/ends-with? file#
                                                                           ::file-name)
                                                (select-keys meta# [:file])))))))
                         {::namespace-sym (symbol ns-name)
                          ::file-name filename})
          {:keys [result]} (eval/evaluate evaluator code {:options {:kind :clj/aux}})]
    result))

(connect/defresolver from-stacktrace [{:keys [ex/filename ex/function-name repl/evaluator
                                              ex/row ex/col]}]
  {::pco/input [:ex/filename :ex/row (pco/? :ex/col) (pco/? :repl/evaluator) (pco/? :ex/function-name)]
   ::pco/output [:definition/filename :definition/row :definition/col :definition/contents]}
  (let [add-pos #(cond-> (assoc % :definition/row row)
                   col (assoc :definition/col col))]
    (cond
      (file-exists? filename)
      (add-pos {:definition/filename filename})

      (and function-name evaluator)
      (p/let [{:keys [file line column]} (clj-stack function-name filename evaluator)
              final-row (or row (dec line))
              final-col (or col (some-> column dec))
              result (from-clj file final-row final-col evaluator)]
        (add-pos result)))))

(connect/defresolver clj-namespace
  [{:keys [q]} {:keys [repl/evaluator text/current-var repl/kind]}]
  {::pco/output [:definition/filename :definition/row :definition/col]}

  (when-let [fqn (and (not= :cljs kind) (-> current-var :text/contents symbol))]
    (when (-> fqn namespace nil?)
      (p/let [code (template `(let [ns# (find-ns '::namespace-sym)
                                    first-var# (some-> ns#
                                                       ns-interns
                                                       first
                                                       second
                                                       meta
                                                       :file)
                                     ns-meta# (meta ns#)]
                                (cond-> ns-meta#
                                        first-var# (assoc :file first-var#)))
                             {::namespace-sym fqn})
              {:keys [result]} (eval/evaluate evaluator code {:options {:kind :clj/aux}})
              file (:file result)
              ns-info (q {:file/filename file :text/language :clojure}
                         [{:file/contents [:text/ns]}])
              [ns-row ns-col] (-> ns-info :file/contents :text/ns :text/range first)]
        (when file
          (p/let [res (from-clj file ns-row ns-col evaluator)]
            (assoc res
                   :definition/row ns-row
                   :definition/col ns-col)))))))

(connect/defresolver cljs-namespace
  [env {:keys [repl/evaluator text/current-var repl/kind repl/cljs-env]}]
  {::pco/output [:definition/filename :definition/row :definition/col]}

  (when-let [fqn (and (= :cljs kind) (-> current-var :text/contents symbol))]
    (when (-> fqn namespace nil?)
      (p/let [code (template `(-> ::cljs-env :cljs.analyzer/namespaces (get '::ns-var-part))
                             {::cljs-env (symbol cljs-env)
                              ::ns-var-part fqn})
              result (eval/evaluate evaluator code {:options {:kind :clj/aux}})]
        (when-let [meta (-> result :result :meta)]
          (p/let [{:keys [file line column]} meta
                  row (dec line)
                  col (some-> column dec)
                  result (from-clj file row col evaluator)]
            (cond-> (assoc result :definition/row row)
              col (assoc :definition/col col))))))))

(connect/defresolver source-from-file [{:definition/keys [filename row col contents]
                                        :text/keys [language]}]
  {::pco/input [:definition/filename :definition/row :text/language
                (pco/? :definition/col) (pco/? :definition/contents)]
   ::pco/output [{:definition/source [:text/contents :text/range]}]}

  (when (= :clojure language)
    (p/let [source (or (:text/contents contents) (helpers/read-file filename))
            top-blocks (helpers/top-blocks source)]
      (when-let [[range text] (helpers/top-block-for top-blocks [row (or col 0)])]
        {:definition/source {:text/contents text
                             :text/range range}}))))

(def resolvers [join-paths-resolver file-exists-resolver
                from-meta from-stacktrace
                clj-namespace cljs-namespace
                source-from-file])
