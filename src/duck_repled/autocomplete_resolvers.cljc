(ns duck-repled.autocomplete-resolvers
  (:require [duck-repled.connect :as connect]
            [com.wsscode.pathom3.connect.operation :as pco]
            [clojure.string :as str]
            [duck-repled.template :as t]
            [promesa.core :as p]
            [orbit.evaluation :as eval]
            [clojure.set :as set]))

(def ^:private valid-prefix #"/?([a-zA-Z0-9\-.$!?\/><*=\?_:]+)")
(def ^:private re-char-escapes
  (->> "\\.*+|?()[]{}$^"
       set
       (map (juxt identity #(str "\\" %)))
       (into {})))
(defn- re-escape [prefix] (str/escape (str prefix) re-char-escapes))

(defn- make-prefix-re [contents]
  (->> contents (re-seq valid-prefix) last last re-escape (str "^") re-pattern))

(connect/defresolver clojure-complete [{:keys [text/contents repl/evaluator
                                               repl/kind repl/namespace]}]
  {::pco/output [:completions/var]}
  (when-not (= kind :cljs)
    (p/let [prefix (make-prefix-re contents)
            cmd `(let [collect# #(map (comp str first) (%1 %2))
                       refers# (collect# clojure.core/ns-map *ns*)
                       from-ns# (->> (clojure.core/ns-aliases *ns*)
                                     (mapcat (fn [[k# v#]]
                                               (map #(str k# "/" %)
                                                    (collect# ns-publics v#)))))]
                   (->> refers#
                        (concat from-ns#)
                        (filter #(re-find :prefix-regexp %))))
            res (eval/evaluate evaluator
                           (t/template cmd {:prefix-regexp prefix})
                           {:namespace namespace :options {:kind :clj/aux}})]
      {:completions/var (mapv (fn [res]
                                {:text/contents res
                                 :completion/type :function})
                              (:result res))})))

(connect/defresolver clojure-keyword [{:keys [text/contents repl/evaluator repl/kind]
                                       ns :repl/namespace}]
  {::pco/output [:completions/keyword]}
  (when-not (= kind :cljs)
    (p/let [prefix (make-prefix-re contents)
            cmd `(let [^java.lang.reflect.Field field# (.getDeclaredField clojure.lang.Keyword "table")]
                   (.setAccessible field# true)
                   (->> (.get field# nil)
                        (map #(.getKey %))))
            {:keys [result]} (eval/evaluate evaluator (t/template cmd {})
                                            {:options {:kind :clj/aux}})
            imports (when (str/starts-with? contents "::")
                      (eval/evaluate evaluator
                                     (t/template `(map #(mapv str %) (clojure.core/ns-aliases *ns*))
                                                 {})
                                     {:namespace ns :options {:kind :clj/aux}}))
            all-nses (cond-> result
                             imports (concat
                                      (for [[alias nss] (:result imports)
                                            one-ns result
                                            :when (symbol? one-ns)
                                            :let [row-ns (namespace one-ns)]
                                            :when (= row-ns nss)]
                                        (str ":" alias "/" (name one-ns)))))]
      {:completions/keyword
       (->> all-nses
            (map #(str ":" %))
            (filter #(re-find prefix %))
            (mapv (fn [elem] {:text/contents elem :completion/type :keyword})))})))

(def ^:private get-completions-tpl
  `(let [curr-ns# (-> ::env
                      :cljs.analyzer/namespaces
                      (get '::namespace))
         filter-map# (fn [e#] (->> e#
                                   (filter #(->> % first str (re-find ::prefix)))
                                   (map (fn [[k# v#]]
                                          {:completion/type (cond
                                                              (:macro v#) :macro
                                                              (:fn-var v#) :function
                                                              :else :var)
                                           :text/contents (str k#)}))))]
     (concat (-> curr-ns# :defs filter-map#)
             (-> curr-ns# :macros filter-map#)
             (->> curr-ns# :requires
                  (filter #(->> % first str (re-find ::prefix)))
                  (map (fn [[k#]] {:text/contents (str k#)
                                   :completion/type :namespace})))
             (->> curr-ns# :uses
                  (filter #(->> % first str (re-find ::prefix)))
                  (map (fn [[k#]] {:text/contents (str k#)
                                   :completion/type :other})))
             (->> curr-ns# :use-macros
                  (filter #(->> % first str (re-find ::prefix)))
                  (map (fn [[k#]] {:text/contents (str k#)
                                   :completion/type :macro}))))))

(defn- ns-alias->fqn [repl cljs-env ns-name ns-alias]
  (p/then (eval/evaluate repl
                         (t/template `(-> ::env
                                          :cljs.analyzer/namespaces
                                          (get '::namespace)
                                          :requires
                                          (get '::namespace-part))
                                     {::env (symbol cljs-env)
                                      ::namespace ns-name
                                      ::namespace-part (symbol ns-alias)})
                         {:options {:kind :clj/aux}})
          :result))

(defn- get-ns-completions! [repl contents cljs-env ns-name]
  (p/let [[ns-part var-part] (str/split contents #"/")
          ; res (repl/eval clj (t/template `(-> ::env
          ;                                     :cljs.analyzer/namespaces
          ;                                     (get '::namespace)
          ;                                     :requires
          ;                                     (get '::namespace-part))
          ;                                {::env (symbol cljs-env)
          ;                                 ::namespace ns-name
          ;                                 ::namespace-part (symbol ns-part)}))
          fqn-ns (ns-alias->fqn repl cljs-env ns-name ns-part)
          comp (eval/evaluate repl (t/template get-completions-tpl
                                               {::env (symbol cljs-env)
                                                ::namespace fqn-ns
                                                ::prefix (make-prefix-re var-part)})
                              {:options {:kind :clj/aux}})]
    (->> comp
         :result
         (map (fn [e] (update e :text/contents #(str ns-part "/" %))))
         (hash-map :result))))

(connect/defresolver cljs-complete [{:keys [text/contents repl/evaluator
                                            repl/kind repl/cljs-env]
                                     ns-name :repl/namespace}]
  {::pco/output [:completions/var]}
  (when (= kind :cljs)
    (p/let [prefix (make-prefix-re contents)
            cmd (t/template get-completions-tpl
                            {::env (symbol cljs-env)
                             ::namespace ns-name
                             ::prefix prefix})
            is-ns-prefix? (re-find #"/" contents)
            additional (if is-ns-prefix?
                         (get-ns-completions! evaluator contents cljs-env ns-name)
                         (eval/evaluate evaluator
                                        (t/template get-completions-tpl
                                                    {::env (symbol cljs-env)
                                                     ::namespace 'cljs.core
                                                     ::prefix prefix})
                                        {:options {:kind :clj/aux}}))
            {:keys [result]} (eval/evaluate evaluator
                                            cmd
                                            {:options {:kind :clj/aux}})]
      {:completions/var (-> result
                            (concat (:result additional))
                            vec)})))

(defn- norm-prefix-regexp [contents curr-ns get-ns!]
  (if (re-find #"^::" contents)
    (if (re-find #"/" contents)
      (p/let [[ns-name var-name] (str/split contents #"/")
              fqn-ns (get-ns! (subs ns-name 2))]
        (if fqn-ns
          [(make-prefix-re (str ":" fqn-ns "/" var-name)) #(str ns-name "/" (name %))]
          [#"::::" identity]))
      [(make-prefix-re (str ":" curr-ns "/" (subs contents 2))) #(str "::" (name %))])
    [(make-prefix-re contents) str]))

(connect/defresolver cljs-kw-complete [{:keys [text/contents repl/evaluator
                                               repl/kind repl/cljs-env]
                                        ns-name :repl/namespace}]
  {::pco/output [:completions/keyword]}
  (when (= kind :cljs)
    (p/let [tpl `(->> ::cljs-env
                      :cljs.analyzer/constant-table
                      (map first)
                      (filter #(->> % str (re-find ::norm-prefix))))
            [prefix-re norm-fn] (norm-prefix-regexp contents
                                                    ns-name
                                                    #(ns-alias->fqn evaluator cljs-env ns-name %))
            matches (eval/evaluate evaluator
                                   (t/template tpl {::cljs-env (symbol cljs-env)
                                                    ::norm-prefix prefix-re})
                                   {:options {:kind :clj/aux}})]
      (->> matches
           :result
           (mapv (fn [kw]
                   {:text/contents (norm-fn kw)
                    :completion/type :keyword}))
           (hash-map :completions/keyword)))))

(def ^:private non-clj-var-regex #"[^a-zA-Z0-9\-.$!?\/><*=\?_:]+")
(defn- normalize-position [top-block-range editor-range]
  (let [[top-start] top-block-range
        [editor-start] editor-range
        [top-row] top-start
        [editor-row] editor-start]
    (if (= top-row editor-row)
      (mapv - editor-start top-start)
      (update editor-start 0 - top-row))))

(defn- get-prefix [text row col]
  (-> text
      str/split-lines
      (nth row "")
      (->> (take col)
           (apply str))
      (str/split non-clj-var-regex)
      last
      str))

(def clj-var-regex-str
  (let [source (.-source non-clj-var-regex)]
    (-> source
        (str/replace-first #"\^" "")
        (subs 0 (- (count source) 2))
        (str "*"))))

(defn- make-context [text prefix row col]
  (let [lines (str/split-lines text)
        pattern (re-pattern (str "(.{" (- col (count prefix)) "})"
                                 clj-var-regex-str))]
    (->> "$1__prefix__"
         (update lines row str/replace-first pattern)
         (str/join "\n"))))

(connect/defresolver compliment [{:keys [text/top-block
                                         text/range
                                         repl/kind
                                         repl/evaluator]
                                  ns-symbol :repl/namespace}]
  {::pco/output [:completions/external]}
  (when (= :clj kind)
    (p/let [text (:text/contents top-block)
            [row col] (normalize-position (:text/range top-block) range)
            prefix (get-prefix text row col)
            context (make-context text prefix row col)
            cmd `(compliment.core/completions
                  ::prefix
                  {:tag-candidates true
                   :ns '::ns
                   :context ::context})
            code (t/template cmd {::prefix prefix
                                  ::ns ns-symbol
                                  ::context context})
            res (eval/evaluate evaluator code {:options {:kind :clj/aux}})]
      (when-let [res (:result res)]
        {:completions/external (mapv #(set/rename-keys % {:candidate :text/contents
                                                          :type :completion/type})
                                     res)}))))

(connect/defresolver nrepl [{:keys [text/current-var text/range
                                    repl/evaluator]
                             ns-symbol :repl/namespace}]
  {::pco/output [:completions/nrepl]}
  (p/let [text (:text/contents current-var)
          [row col] (normalize-position (:text/range current-var) range)
          prefix (get-prefix text row col)
          res (eval/evaluate evaluator
                             {:prefix prefix
                              :ns (str ns-symbol)}
                             {:options {:kind :clj/aux :op "completions"}})]
    (when-let [res (:result res)]
      {:completions/nrepl (->> (get res "completions")
                               (mapv (fn [res]
                                       {:text/contents (get res "candidate")
                                        :completion/type (-> res (get "type") keyword)})))})))

(connect/defresolver suitable [{:keys [text/top-block text/range
                                       repl/kind repl/cljs-env repl/cljs-eval
                                       repl/evaluator]
                                ns-symbol :repl/namespace}]
  {::pco/output [:completions/external]}
  (when (= :cljs kind)
    (p/let [text (:text/contents top-block)
            [row col] (normalize-position (:text/range top-block) range)
            prefix (get-prefix text row col)
            context (make-context text prefix row col)
            cmd `(binding [suitable.compliment.sources.cljs/*compiler-env* ::env]
                   (compliment.core/completions
                    ::prefix
                    {:tag-candidates true
                     :ns '::ns
                     :sources [:suitable.compliment.sources.cljs/cljs-source
                               :compliment.sources.local-bindings/local-bindings]
                     :context ::context}))
            code (t/template cmd {::prefix prefix
                                  ::ns ns-symbol
                                  ::env (symbol cljs-env)
                                  ::context context})
            suitable-cmd (t/template `(suitable.js-completions/cljs-completions
                                       ~cljs-eval
                                       ::prefix
                                       {:ns ::ns
                                        :context ::context})
                                     {::prefix prefix
                                      ::ns (str ns-symbol)
                                      ::context context})
            compliment (p/then (eval/evaluate evaluator code {:options {:kind :clj/aux}})
                               :result)
            suitable (p/then (eval/evaluate evaluator suitable-cmd {:options {:kind :clj/aux}})
                             :result)]
      (when-let [res (not-empty (concat compliment suitable))]
        {:completions/external (mapv #(-> %
                                          (set/rename-keys {:candidate :text/contents
                                                            :type :completion/type})
                                          (update :completion/type (fn [t]
                                                                     (cond-> t (string? t) keyword))))
                                     res)}))))

(def resolvers [clojure-complete clojure-keyword
                cljs-complete cljs-kw-complete
                compliment suitable nrepl])
