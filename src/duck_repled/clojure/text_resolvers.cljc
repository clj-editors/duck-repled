(ns duck-repled.clojure.text-resolvers
  (:require [clojure.string :as str]
            [duck-repled.schemas :as schemas]
            [duck-repled.connect :as connect]
            [com.wsscode.pathom3.connect.operation :as pco]
            [duck-repled.editor-helpers :as editor-helpers]))

(connect/defresolver namespace-from-text [{:text/keys [top-blocks range language]}]
  {::pco/output [{:text/ns [:text/contents :text/range :text/ns]}]}

  (when (= :clojure language)
    (when-let [[range ns] (editor-helpers/ns-range-for top-blocks (first range))]
      {:text/ns {:text/contents (str ns) :text/range range}})))

(connect/defresolver contents-top-blocks [{:text/keys [contents language]}]
  {::pco/output [:text/top-blocks]}
  (when (= :clojure language)
    {:text/top-blocks (editor-helpers/top-blocks contents)}))

(connect/defresolver contents-top-block [{:text/keys [top-blocks range ns]}]
  {::pco/input [:text/top-blocks :text/range (pco/? :text/ns) (pco/? :file/filename)]
   ::pco/output [{:text/top-block [:text/contents :text/range :text/ns :repl/watch-point]}]}

  (when-let [[range text] (editor-helpers/top-block-for top-blocks (first range))]
    {:text/top-block (cond-> {:text/contents text :text/range range :repl/watch-point ""}
                       ns (assoc :text/ns ns))}))

(connect/defresolver text-block [{:text/keys [contents range ns language]}]
  {::pco/input [:text/contents :text/range (pco/? :text/ns) (pco/? :file/filename)]
   ::pco/output [{:text/block [:text/contents :text/range :text/ns]}]}
  (when (= :clojure language)
    (when-let [[range text] (editor-helpers/block-for contents (first range))]
      {:text/block (cond-> {:text/contents text :text/range range}
                     ns (assoc :text/ns ns))})))

(connect/defresolver resolver-for-ns [inputs]
  {::pco/input [:text/language (pco/? :text/ns) (pco/? :repl/kind) (pco/? :file/filename)]
   ::pco/output [:repl/namespace]}

  (when (= :clojure (:text/language inputs))
    (let [contents (or (-> inputs :text/ns :text/contents)
                       (-> inputs :editor/contents :text/ns :text/contents))
          kind (:repl/kind inputs)]
      (cond
        contents {:repl/namespace (symbol contents)}
        (nil? kind) nil
        (= :cljs kind) {:repl/namespace 'cljs.user}
        :not-cljs {:repl/namespace 'user}))))

(connect/defresolver resolve-repl-kind
  [{:keys [config/repl-kind config/eval-as file/filename]}]
  {::pco/input [(pco/? :config/repl-kind) (pco/? :config/eval-as)
                (pco/? :file/filename)]
   ::pco/output [:repl/kind]}

  (let [filename (str filename)]
    (cond
      (and repl-kind (not= :clj repl-kind))
      {:repl/kind repl-kind}

      (#{:clj :cljs} eval-as)
      {:repl/kind eval-as}

      :else
      (let [cljs-file? (str/ends-with? filename ".cljs")
            cljc-file? (or (str/ends-with? filename ".cljc")
                           (str/ends-with? filename ".cljx"))]
        (case eval-as
          :prefer-clj {:repl/kind (if cljs-file? :cljs :clj)}
          :prefer-cljs {:repl/kind (if (and (not cljs-file?) (not cljc-file?))
                                     :clj
                                     :cljs)}
          nil)))))

(connect/defresolver var-from-text [{:text/keys [contents range ns language]}]
  {::pco/input [:text/contents :text/range :text/language (pco/? :text/ns)]
   ::pco/output [{:text/current-var [:text/contents :text/range :text/ns]}]}

  (when (= :clojure language)
    (when-let [[range curr-var] (editor-helpers/current-var contents (first range))]
      {:text/current-var (cond-> {:text/contents curr-var :text/range range}
                           ns (assoc :text/ns ns))})))

(def resolvers [resolver-for-ns resolve-repl-kind
                ; BLOCKS
                text-block contents-top-blocks contents-top-block
                var-from-text namespace-from-text])
