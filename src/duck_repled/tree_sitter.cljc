(ns duck-repled.tree-sitter
  (:require ["path" :as path]
            [promesa.core :as p]))

;; TODO: How do I do this in pure Java?
(def ts-path (path/join js/__dirname "tree-sitter"))
(defonce Parser (js/require ts-path))
(defonce initialized
  (.init Parser #js {:locateFile #(path/join js/__dirname "tree-sitter.wasm")}))

(def done
  (p/do!
   initialized
   (p/let [my-path js/__dirname
           ruby (.. Parser -Language (load (path/join my-path "ruby.wasm")))]
     (def ruby-language ruby)
     (def parser (new Parser))
     (.setLanguage parser ruby-language)
     (def ^js parent-query (.query ruby "
(module) @parent (class) @parent
(method) @method (singleton_method) @singleton"))
     (def ^js method-name-query (.query ruby (str "(method name: (_) @name) @method "
                                                  "(singleton_method name: (_) @name) @method")))
     (def any-query (.query ruby "(_) @any")))))

(defn parse [text] (.parse parser text))

(defn root [^js parsed] (.-rootNode parsed))

(defn captures [^js node, ^js query, start, end]
  (let [captures (.captures query node (clj->js start) (clj->js end))]
    (filter (fn [^js capture]
              (let [node (.-node capture)
                    n-start (.-startPosition node)
                    starts? (or (< (.-row n-start) (:row start))
                                (and (= (.-row n-start) (:row start))
                                     (<= (.-column n-start (:column start)))))]
                starts?))
            captures)))
