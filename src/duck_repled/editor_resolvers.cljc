(ns duck-repled.editor-resolvers
  (:require [clojure.string :as str]
            [duck-repled.schemas :as schemas]
            [duck-repled.connect :as connect]
            [com.wsscode.pathom3.connect.operation :as pco]
            [duck-repled.editor-helpers :as editor-helpers]))

(connect/defresolver separate-data [{editor-data :editor/data}]
  {::pco/output [:file/filename
                 :text/language
                 {:editor/contents [:text/contents :text/range]}]}

  (let [file (:filename editor-data)]
    (cond-> {:editor/contents {:text/contents (:contents editor-data)
                               :text/range (:range editor-data)}
             :text/language (:language editor-data)}
            file (assoc :file/filename file))))

(connect/defresolver text-selection [{:text/keys [contents range ns]}]
  {::pco/input [:text/contents :text/range (pco/? :text/ns)]
   ::pco/output [{:text/selection [:text/contents :text/range :text/ns]}]}

  (when-let [text (editor-helpers/text-in-range contents range)]
    {:text/selection (cond-> {:text/contents text :text/range range}
                             ns (assoc :text/ns ns))}))

(connect/defresolver contents-from-filename [env {:file/keys [filename]}]
  {::pco/input [:file/filename]
   ::pco/output [{:file/contents [:text/contents :text/range :file/filename]}]}

  (let [contents (editor-helpers/read-file filename)
        range (-> env pco/params (:range [[0 0] [0 0]]))]
    (when contents
      {:file/contents {:text/range range
                       :text/contents contents
                       :file/filename filename}})))

(connect/defresolver repl-elements-for-blocks [inputs]
  {::pco/input [(pco/? :repl/namespace) (pco/? :file/name)
                (pco/? {:text/top-block [:text/contents :text/range]})
                (pco/? {:text/block [:text/contents :text/range]})
                (pco/? {:text/current-var [:text/contents :text/range]})
                (pco/? {:text/ns [:text/contents :text/range]})]
   ::pco/output [{:text/top-block [:repl/namespace :file/name]}
                 {:text/block [:repl/namespace :file/name]}
                 {:text/current-var [:repl/namespace :file/name]}
                 {:text/ns [:file/name]}]
   ::pco/priority -1}
  (prn :WAT???)
  (let [template (select-keys inputs [:repl/namespace :file/name])]
    (cond-> {}
      (:text/top-block inputs) (assoc :text/top-block template)
      (:text/block inputs) (assoc :text/block template)
      (:text/current-var inputs) (assoc :text/current-var template)
      (:text/ns inputs) (assoc :text/ns template))))

(def resolvers [separate-data text-selection contents-from-filename])
