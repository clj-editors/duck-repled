# Change Log

## 0.1.2
- Adding resolvers to autocomplete (without plug-ins)

## 0.1.1
- Adding resolvers for definition
- Adding resolver for `:definition/source`
