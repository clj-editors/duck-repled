(ns duck-repled.python.blocks-test
  (:require [clojure.test :refer [deftest]]
            [check.async :refer [check testing async-test]]
            [duck-repled.core :as core]
            [promesa.core :as p]))

(def source "class SomeClass(object):
  def some_method(self, a):
    c = a + 20
    return c

  def top_method(self, a):
    def other_method(b):
      return a + b
    a = a + 1
    return other_method(20)")

(defn- top-block-resolver [range]
  (p/let [eql (core/gen-eql)
          res (eql {:text/contents source :text/range range :text/language :python}
                   [:text/top-block])]
    (:text/top-block res)))

(deftest top-blocks
  (async-test "extract top-block from a Python file"
    (testing "extracts the whole class"
      (check (top-block-resolver [[4 0] [4 0]])
             => {:text/contents source
                 :text/range [[0 0] [9 27]]
                 :repl/watch-point ""})

      (check (top-block-resolver [[0 0] [0 0]])
             => {:text/contents source
                 :text/range [[0 0] [9 27]]
                 :repl/watch-point ""}))

    (testing "extracts a method inside a class and rewrite it as a patch"
      (check (top-block-resolver [[1 4] [1 4]])
             => {:text/contents "def some_method(self, a):
    c = a + 20
    return c
SomeClass.some_method = some_method"
                 :text/range [[1 2] [3 12]]
                 :repl/watch-point ""}))

    (testing "extracts a method inside a method and use a watch point"
      (check (top-block-resolver [[6 6] [6 6]])
             => {:text/contents "def other_method(b):
      return a + b"
                 :text/range [[6 4] [7 18]]}))))

(defn- block-resolver [text range]
  (p/let [eql (core/gen-eql)
          res (eql {:text/contents text :text/range range :text/language :python}
                   [:text/block])]
    (:text/block res)))

(deftest blocks
  (async-test "extract blocks from a Python file"
    (testing "checks single elements like primitives, arrays..."
      (check (block-resolver "20" [[0 1] [0 1]]) => {:text/contents "20"
                                                     :text/range [[0 0] [0 2]]})
      (check (block-resolver "\"foo\"" [[0 0] [0 0]]) => {:text/contents "\"foo\""
                                                          :text/range [[0 0] [0 5]]})
      (check (block-resolver "[1, 2, 3]" [[0 0] [0 0]]) => {:text/contents "[1, 2, 3]"
                                                            :text/range [[0 0] [0 9]]})
      (check (block-resolver "{'a': 1, 'b': 2}" [[0 0] [0 0]]) => {:text/contents "{'a': 1, 'b': 2}"
                                                                   :text/range [[0 0] [0 16]]})
      (check (block-resolver "foobar" [[0 0] [0 0]]) => {:text/contents "foobar"
                                                         :text/range [[0 0] [0 6]]})
      (check (block-resolver "foobar(1, 2)" [[0 0] [0 0]]) => {:text/contents "foobar(1, 2)"
                                                               :text/range [[0 0] [0 12]]})
      (check (block-resolver "(1, 2)" [[0 0] [0 0]]) => {:text/contents "(1, 2)"
                                                         :text/range [[0 0] [0 6]]}))

    (testing "arithmetic"
      (check (block-resolver "1 + 2" [[0 0] [0 0]]) => {:text/contents "1 + 2"})
      (check (block-resolver "1 + 2" [[0 2] [0 2]]) => {:text/contents "1 + 2"})
      (check (block-resolver "a == 2" [[0 0] [0 0]]) => {:text/contents "a == 2"}))

    (testing "captures specific elements in single-line multi-command situations"
      (check (block-resolver "foo; bar" [[0 1] [0 1]]) => {:text/contents "foo"
                                                           :text/range [[0 0] [0 3]]})
      (check (block-resolver "foo; bar" [[0 6] [0 6]]) => {:text/contents "bar"
                                                           :text/range [[0 5] [0 8]]}))

    (testing "captures assignments"
      (check (block-resolver "a = 10" [[0 4] [0 4]]) => {:text/contents "a = 10"
                                                         :text/range [[0 0] [0 6]]})
      (check (block-resolver "a, b = 1, 10" [[0 8] [0 8]]) => {:text/contents "a, b = 1, 10"
                                                               :text/range [[0 0] [0 12]]}))

    (testing "captures multi-line statements"
      (check (block-resolver "a = foo.\n  bar(10).\n  baz(20)" [[1 0] [1 0]])
             => {:text/contents "foo.\n  bar(10)" :text/range [[0 4] [1 9]]})
      (check (block-resolver "a = foo.\n  bar(10).\n  baz(20)" [[1 4] [1 4]])
             => {:text/contents "foo.\n  bar(10)" :text/range [[0 4] [1 9]]})
      (check (block-resolver "a = foo.\n  bar(10).\n  baz(20)" [[2 1] [2 1]])
             => {:text/contents "a = foo.\n  bar(10).\n  baz(20)" :text/range [[0 0] [2 9]]})

      (check (block-resolver "b = lol\na = foo.\n  bar(10)" [[2 9] [2 9]])
             => {:text/contents "a = foo.\n  bar(10)" :text/range [[1 0] [2 9]]}))

    (testing "captures multi-line statements when cursor is in params"
      (check (block-resolver "a = foo(\n    a,\n    b\n  ).\n  bar(\n    a,\n    b)" [[0 1] [0 1]])
             => {:text/contents "a" :text/range [[0 0] [0 1]]})
      (check (block-resolver "a = foo(\n    a,\n    b\n  ).\n  bar(\n    a,\n    b)" [[1 0] [1 0]])
             => {:text/contents "foo(\n    a,\n    b\n  )"})
      (check (block-resolver "a = foo(\n    a,\n    b\n  ).\n  bar(\n    a,\n    b)" [[2 0] [2 0]])
             => {:text/contents "foo(\n    a,\n    b\n  )"})
      (check (block-resolver "a = foo(\n    a,\n    b\n  ).\n  bar(\n    a,\n    b)" [[4 0] [4 0]])
             => {:text/contents "a = foo(\n    a,\n    b\n  ).\n  bar(\n    a,\n    b)"})
      (check (block-resolver "a = foo(\n    a,\n    b\n  ).\n  bar(\n    a,\n    b)" [[5 5] [5 5]])
             => {:text/contents "a = foo(\n    a,\n    b\n  ).\n  bar(\n    a,\n    b)"}))

    (testing "avoid capturing the whole method body"
      (check (block-resolver "def whatever():\n  a = 10\n  b = 20\n  a.to_s(b)" [[3 4] [3 4]])
             => {:text/contents "a.to_s(b)" :text/range [[3 2] [3 11]]})
      (check (block-resolver "def whatever():\n  a = 10\n  b = 20\n  print(a.
    to_s(b))"
                             [[3 4] [3 4]])
             => {:text/contents "print(a.\n    to_s(b))" :text/range [[3 2] [4 12]]}))))

(deftest selection
  (async-test "de-indent code when getting selections"
    (p/let [eql (core/gen-eql)]
      (check (eql {:text/contents source
                   :text/range [[1 0] [2 14]]
                   :text/language :python}
                  [:text/selection])
             => {:text/selection {:text/contents "def some_method(self, a):\n  c = a + 20"
                                  :text/range [[1 0] [2 14]]}}))))
