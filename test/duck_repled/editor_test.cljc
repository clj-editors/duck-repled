(ns duck-repled.editor-test
  (:require [check.async :refer [check testing async-test]]
            [clojure.test :refer [deftest]]
            [duck-repled.core :as core]
            [promesa.core :as p]))

(def eql (core/gen-eql))
(deftest editor-data
  (async-test "separates editor data into fragments" {:timeout 8000}
    (check (eql {:editor/data {:contents "(+ 1 2)"
                               :filename nil
                               :language :clojure
                               :range [[0 0] [0 0]]}}
                [:editor/contents :text/language])
           => {:editor/contents {:text/contents "(+ 1 2)"
                                 :text/range [[0 0] [0 0]]}
               :text/language :clojure})

    (check (eql {:editor/data {:contents "(+ 1 2)"
                               :filename "foo.clj"
                               :language :clojure
                               :range [[0 0] [0 0]]}}
                [:file/filename :editor/contents])

           => {:editor/contents {:text/contents "(+ 1 2)"
                                 :text/range [[0 0] [0 0]]}
               :file/filename "foo.clj"})
    (check (eql {:text/contents "(ns lol)(+ 1 2)\n\n( \n ( 2 3))"
                 :text/range [[3 3] [3 7]]}
                [{:text/selection [:text/contents :text/range]}])
           => {:text/selection {:text/range [[3 3] [3 7]]
                                :text/contents "2 3))"}})))

(deftest read-file-contents
  (async-test "read file contents"
    (check (eql {:file/filename "test/duck_repled/tests.cljs"}
                [{'(:file/contents {:patch {:text/range [[33 0] [33 0]]}})
                  [:text/contents :text/range]}])
           => {:file/contents {:text/contents #"^\(ns duck"
                               :text/range [[33 0] [33 0]]}})))
