(ns duck-repled.definition-test
  (:require [check.async :refer [check testing async-test]]
            [clojure.test :refer [deftest]]
            [duck-repled.core :as core]
            [promesa.core :as p]
            [duck-repled.repl-helpers :as helpers]
            [orbit.evaluation :as eval]))

(def eql (core/gen-eql))

(deftest existing-file-checkers
  (async-test "finds if file exists"
    (check (eql {:file/path ["test" "duck_repled" "tests.cljs"]}
                [:file/filename :file/exists?])
           => {:file/filename "test/duck_repled/tests.cljs"
               :file/exists? true})

    (testing "finds existing filename by meta"
      (check (eql {:var/meta {:file "test/duck_repled/tests.cljs"}}
                  [:definition/filename])
             => {:definition/filename "test/duck_repled/tests.cljs"}))

    (testing "finds existing filename by stacktrace info"
      (check (eql {:ex/filename "test/duck_repled/tests.cljs" :ex/row 10}
                  [:definition/filename])
             => {:definition/filename "test/duck_repled/tests.cljs"}))))

(deftest simple-definition-finding
  (async-test "find definition row, and filename for a var using its meta"
    (helpers/with-orbit-excluding-kinds [repl] #{:bb :org.babashka/nbb}
      (p/let [seed {:repl/evaluator repl
                    :editor/data {:contents "(ns foo)\nmy-fun"
                                  :filename "file.clj"
                                  :language :clojure
                                  :range [[1 0] [1 0]]}
                    :config/eval-as :prefer-clj}
              support-line? (eval/evaluate repl "#?(:bb false :org.babashka/nbb false :default true)")]

        (eval/evaluate repl
                        "\n(defn my-fun \"My doc\" [])"
                        {:filename "test/duck_repled/tests.cljs"
                         :row 0
                         :namespace 'foo :options {:kind :clj/eval}})

        (let [to-match (cond-> {:definition/filename "test/duck_repled/tests.cljs"}
                         support-line? (assoc :definition/row 1))]
          (check (eql seed [{:editor/contents [:definition/filename :definition/row]}])
                 =>
                 {:editor/contents to-match}))))))

(deftest resolving-filenames-in-clj
  (helpers/with-orbit-including-kinds [repl] #{:clj}
    (p/let [seed {:repl/evaluator repl
                  :editor/data {:contents "(ns foo)\nstr/replace"
                                :language :clojure
                                :range [[1 0] [1 0]]}
                  :config/eval-as :prefer-clj}]

      (testing "finds JAR and unpacks in CLJ and CLJS funcions"
        (check (eql (assoc-in seed [:editor/data :filename] "file.clj")
                    [{:editor/contents [:definition/filename :definition/contents]}])
               => {:editor/contents
                   {:definition/filename #"clojure.*jar!/clojure/string.clj"
                    :definition/contents {:text/contents #"clojure.string"
                                          :text/range [[74 0] [74 0]]}}})

        (check (eql (assoc-in seed [:editor/data :filename] "file.cljs")
                    [{:editor/contents [:definition/filename :definition/contents]}])
               => {:editor/contents
                   {:definition/filename #"clojure.*jar!/clojure/string.cljs"
                     :definition/contents {:text/contents #"clojure.string"
                                           :text/range [[#(>= % 43) 0]
                                                        [#(>= % 43) 0]]}}}))

      (testing "getting path of stacktrace"
        (check (eql (-> seed
                        (assoc-in [:editor/data :filename] "file.clj")
                        (assoc-in [:editor/data :contents] "str\nstr")
                        (assoc :ex/function-name "clojure.string/fn/eval1234"
                               :ex/filename "string.clj"
                               :ex/row 7
                               :ex/col 8))
                    [:definition/filename :definition/row :definition/col])
               => {:definition/row 7
                   :definition/col 8
                   :definition/filename #"clojure.*jar!/clojure/string.clj"})))))

#_
(deftest resolving-filenames-in-cljr
  (async-test "resolves filenames and contents, if internal from CLR"
    (when (= :cljr helpers/*kind*)
      (p/let [repl (helpers/prepare-repl helpers/*global-evaluator*)
              seed {:repl/evaluators {:clj repl}
                    :repl/kind :cljr
                    :editor/data {:contents "(ns foo)\nstr/replace"
                                  :filename "file.clj"
                                  :range [[1 0] [1 0]]}
                    :config/eval-as :prefer-clj}]
        (check (eql seed [:definition/filename :definition/row])
               => {:definition/filename #"clojure.main.*string.clj"
                   :definition/row number?})))))

(deftest source-for-var
  (async-test "read file contents, and get top block of var" {:timeout 10000}
    (helpers/with-orbit-including-kinds [repl] #{:clj}
      (p/let [seed {:repl/evaluator repl
                    :editor/data {:contents "(ns foo)\nstr/replace"
                                  :filename "file.clj"
                                  :language :clojure
                                  :range [[1 0] [1 0]]}
                    :config/repl-kind :clj
                    :config/eval-as :prefer-clj}]

        (testing "will get source inside a CLJ file"
          (p/let [res (eql seed [{:editor/contents [:definition/source]}])
                  is-clj? (eval/evaluate repl "#?(:bb false :clj true :default false)")]
            (check res => {:editor/contents
                           {:definition/source
                             {:text/contents #".*defn.*replace"}}})
            (when is-clj?
              (check res => {:editor/contents
                             {:definition/source
                               {:text/contents #"java\.util\.regex"}}}))))

        (when @helpers/cljs?
          (testing "will get source inside a CLJS file"
            (p/let [res (eql (update-in seed [:editor/data :filename] str "s")
                             [{:editor/contents [:definition/source]}])]
              (check res => {:editor/contents
                             {:definition/source
                               {:text/contents #".*defn.*replace"}}})
              (check res => {:editor/contents
                             {:definition/source
                               {:text/contents #(not (re-find #"java\.util\.regex" %))}}}))))))))

(deftest ns-definition
  (async-test "find definition of a namespace"
    (helpers/with-orbit-including-kinds [repl] #{:clj}
      (p/let [seed {:repl/evaluator repl
                    :editor/data {:contents "(ns custom-ns)\nmy-fun"
                                  :filename "file.clj"
                                  :language :clojure
                                  :range [[0 5] [0 5]]}
                    :config/eval-as :prefer-clj}]

        (testing "with Clojure REPLs"
          (eval/evaluate repl
                         "(ns custom-ns (:require [clojure.string :as str]))\n(defn my-fun \"My doc\" [])"
                         {:filename "test/duck_repled/tests.cljs"
                          :options {:kind :clj/eval}})

          (check (eql seed [{:editor/contents [:definition/filename :definition/row :definition/col]}])
                 => {:editor/contents
                     {:definition/filename "test/duck_repled/tests.cljs"
                       :definition/row 0
                       :definition/col 0}}))

        (when @helpers/cljs?
          (testing "with ClojureScript REPLs"
            (check (eql (update seed :editor/data merge {:filename "file.cljs"
                                                         :contents "(ns a (:require [clojure.string :as str]))"
                                                         :range [[0 20] [0 20]]})
                        [{:editor/contents [:definition/filename :definition/row :definition/col]}])
                   => {:editor/contents
                       {:definition/filename #"clojurescript.*string\.cljs"
                         :definition/row 8
                         :definition/col 4}})))))))
