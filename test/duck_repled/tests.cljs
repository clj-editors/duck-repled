(ns duck-repled.tests
  (:require [duck-repled.editor-test]
            [duck-repled.repl-test]
            [duck-repled.definition-test]
            [duck-repled.autocomplete-test]
            [duck-repled.core-test]
            [clojure.test :as test]
            [promesa.core :as p]
            [orbit.nrepl.evaluator :as nrepl]
            [duck-repled.repl-helpers :as helpers]
            [duck-repled.tree-sitter :as ts]
            [duck-repled.ruby.blocks-test]
            [duck-repled.clojure.blocks-and-namespaces-test]
            [duck-repled.ruby.feature-test]))

;;; White spaces to accomodate new tests







(defmethod test/report [::test/default :begin-test-var] [m]
  (println "Testing:" (test/testing-vars-str m)))

(defn- run-tests [] (test/run-all-tests #"duck-repled.*-test"))
(defn- run-ruby-tests [] (test/run-all-tests #"duck-repled.*ruby.*-test"))

(defn main [ & args]
  (when (-> args count (> 1))
    (reset! helpers/host (-> args (nth 1)))
    (reset! helpers/nrepl-port (-> args (nth 2) js/parseInt))
    (reset! helpers/connect-fn (fn [host port _] (nrepl/connect! host port identity)))
    (when-let [lang (nth args 3 nil)]
      (reset! helpers/language (keyword lang))
      (reset! helpers/connect-fn (fn [host port _]
                                   (nrepl/connect! host port identity {:serdes {:serialize identity
                                                                                :deserialize #(hash-map :result %)}})))))

  (when (-> args first (= "--test"))
    (defmethod test/report [::test/default :summary] [{:keys [test pass fail error]}]
      (println "Ran" test "tests containing" (+ pass fail error) "assertions.")
      (println pass "passed," fail "failures," error "errors.")
      (if (= 0 fail error)
        (js/process.exit 0)
        (js/process.exit 1)))
    (p/do!
     ts/done
     (case (nth args 3 "clj")
       "ruby" (run-ruby-tests)
       (run-tests))))

  (when (= [] args)
    (prn :loaded))

  ; More spaces because this is used in a test (this block should end at line 60)


  ,,,)
