(ns duck-repled.test-helpers
  #?(:cljs (:require-macros [duck-repled.test-helpers])))

(defmacro with-eql [description binding & body]
  (let [binding (conj binding `(duck-repled.core/gen-eql))]
    `(let ~binding
       (check.async/async-test ~description
         ~@body))))
