(ns duck-repled.repl-test
  (:require [check.async :refer [check testing async-test]]
            [clojure.test :refer [deftest]]
            [duck-repled.core :as core]
            [promesa.core :as p]
            [duck-repled.repl-helpers :as helpers]
            [orbit.evaluation :as eval]))

(def eql (core/gen-eql))
(deftest repl-definition
  (async-test "will run on CLJ or CLJS REPL depending on what's expected" {:timeout 8000}
    (helpers/with-orbit-repl! [repl]
      (p/let [seed {:repl/evaluator repl
                    :editor/data {:contents "(ns foo)\nflavor"
                                  :language :clojure
                                  :range [[1 0] [1 0]]}
                    :config/eval-as :prefer-clj}]

        (eval/evaluate repl "(ns foo)")
        (eval/evaluate repl "(def flavor :clj)" {:namespace 'foo
                                                 :options {:kind :clj/eval}})

        (when @helpers/cljs?
          (eval/evaluate repl "(def flavor :cljs)" {:namespace 'foo
                                                    :options {:kind :cljs/eval}}))

        (testing "will use Clojure REPL"
          (check (eql (assoc-in seed [:editor/data :filename] "file.clj")
                      [{:editor/contents [{:text/current-var [:repl/result]}]}])
                 => {:editor/contents {:text/current-var {:repl/result {:result :clj}}}}))

        (when @helpers/cljs?
          (testing "will use ClojureScript REPL"
            (check (eql (assoc-in seed [:editor/data :filename] "file.cljs")
                        [{:editor/contents [{:text/current-var [:repl/result]}]}])
                   => {:editor/contents {:text/current-var {:repl/result {:result :cljs}}}})))))))


(. js/process on "unhandledRejection" (fn [reason promise]
                                        (prn [:uncaught-error reason])
                                        (def reason reason)
                                        (def promise promise)))

(deftest eval-commands
  (async-test "will run on CLJ or CLJS REPL depending on what's expected" {:timeout 8000}
    (helpers/with-orbit-repl! [repl]
      (testing "evaluates command"
        (check (eql {:repl/evaluator repl
                     :text/contents "(+ 1 2)"
                     :text/language :clojure}
                    [:repl/result])
               => {:repl/result {:result 3}})

        (check (eql {:repl/evaluator repl
                     :text/language :clojure
                     :text/contents "(ex-info)"}
                    [:repl/error])
               => {:repl/error {:error any?}}))

      (testing "sends ROW/COL and NS if available"
        (check (eql {:repl/evaluator repl :repl/namespace 'foo
                     :file/filename "file.clj"
                     :text/language :clojure
                     :text/contents "some-var" :text/range [[2 4] [4 5]]}
                    [:repl/result])
               => {:repl/result {:result 10}}))

      (testing "applies template/options to code and evaluate"
        (check (eql {:repl/evaluator repl :text/contents "(+ 1 2)" :text/language :clojure}
                    ['(:repl/result {:repl/template (inc :repl/code)
                                     :row 20})])
               => {:repl/result {:result 4}}))

      (testing "evaluates fragments of editor in REPL"
        (p/let [editor {:filename "foo.clj"
                        :contents "(ns foo)\n(+ 1 2)\n(-  (+ 3 4)\n    (+ 5 some-var))"
                        :language :clojure
                        :range [[3 7] [3 8]]}]
          (check (eql {:editor/data editor :repl/evaluator repl :text/language :clojure}
                      [{:editor/contents
                        [{:text/selection [:repl/result]}
                         {:text/block [:repl/result]}
                         {:text/top-block [:repl/result]}]}])
                 => {:editor/contents
                     {:text/selection {:repl/result {:result 5}}
                      :text/block {:repl/result {:result 15}}
                      :text/top-block {:repl/result {:result -8}}}}))))))

(deftest getting-infos-about-vars
  (async-test "will get full-qualified name of a var" {:timeout 8000}
    (helpers/with-orbit-repl! [repl]
      (p/let [seed {:repl/evaluator repl
                    :editor/data {:contents "(ns foo)\nmy-fun"
                                  :filename "file.clj"
                                  :language :clojure
                                  :range [[1 0] [1 0]]}
                    :config/eval-as :prefer-clj}]
        (testing "will find fqn for current var"
          (check (eql seed [{:editor/contents [:var/fqn]}])
                 => {:editor/contents {:var/fqn 'foo/my-fun}}))))))

(deftest getting-meta-and-dependent
  (when @helpers/cljs?
    (async-test "will run on CLJ or CLJS REPL depending on what's expected" {:timeout 8000}
      (helpers/with-orbit-repl! [repl]
        (p/let [seed {:repl/evaluator repl
                      :editor/data {:contents "(ns foo)\nmy-fun"
                                    :filename "file.clj"
                                    :language :clojure
                                    :range [[1 0] [1 0]]}
                      :config/eval-as :prefer-clj}]
          (eval/evaluate repl "(defn my-clj-fun \"Another doc\" [] (+ 1 2))\n"
                         {:namespace 'foo :options {:kind :clj/aux}})

          (testing "will get metadata of a var"
            (check (eql seed [{:editor/contents [:var/meta]}])
                   => {:editor/contents {:var/meta {:doc "My doc"}}}))

          (testing "will get metadata of a var using CLJS"
            (check (eql (-> seed
                            (assoc :repl/kind :cljs :config/repl-kind :clj))
                        [{:editor/contents [:var/meta]}])
                   => {:editor/contents {:var/meta {:doc "My doc"}}}))

          (testing "will get metadata of macros in CLJS"
            (check (eql (-> seed
                            (assoc :repl/kind :cljs :config/repl-kind :clj)
                            (assoc-in [:editor/data :contents] "(ns foo)\nfor"))
                        [{:editor/contents [:var/meta]}])
                   => {:editor/contents {:var/meta {:doc #"List comprehension"}}}))

          #_
          (testing "will get DOC for that current var"
            (check (eql (-> seed
                            (assoc :repl/kind :cljs :config/repl-kind :clj)
                            (assoc-in [:editor/data :contents] "(ns foo)\nmy-clj-fun"))
                        [:var/doc])
                   => {:var/doc (str "-------------------------\n"
                                     "foo/my-clj-fun\n"
                                     "([])\n"
                                     "  Another doc")}))))))

  #_
  (when (#{:clje} helpers/*kind*)
    (async-test "will coerce infos from #erl maps" {:timeout 8000}
      (p/let [evaluator (helpers/prepare-repl helpers/*global-evaluator*)
              seed {:repl/evaluators {:clj evaluator}
                    :editor/data {:contents "(ns foo)\nmy-fun"
                                  :filename "file.clj"
                                  :range [[1 0] [1 0]]}
                    :config/repl-kind :clje
                    :config/eval-as :prefer-clj}]
        (p/do!
         (testing "will get metadata of a var"
           (check (eql seed [:var/meta]) => {:var/meta {:doc "My doc"}}))

         (testing "will get DOC for that current var"
           (check (eql seed [:var/doc])
                  => {:var/doc (str "-------------------------\n"
                                    "foo/my-fun\n"
                                    "([])\n"
                                    "  My doc")})))))))
