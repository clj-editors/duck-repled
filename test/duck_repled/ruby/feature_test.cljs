(ns duck-repled.ruby.feature-test
  (:require [duck-repled.repl-helpers :refer [with-plain-repl with-eql language]]
            [matcher-combinators.matchers :as m]
            [clojure.test :refer [deftest]]
            [check.async :refer [testing check]]
            [orbit.evaluation :as eval]
            [promesa.core :as p]))

(def symbol-code
  "Symbol.all_symbols.lazy.select { |x| x.inspect =~ /f.*o/ }.select { |x| x.inspect !~ /\\\\x/ }.map { |x| ['symbol', x.to_s + ':'] }.to_a")

(deftest complete-simple-fields
  (with-eql "code to complete ruby" [eql]
    (testing "getting local variables"
      (check (eql {:text/contents "fo" :text/range [[0 2] [0 2]] :text/language :ruby}
                  [:completions/code])
             => {:completions/code (str "binding.local_variables.map { |m| ['local_var', m.to_s] } + "
                                        "public_methods.map { |m| ['pub_method', m.to_s] } + "
                                        "private_methods.map { |m| ['priv_method', m.to_s] } + "
                                        "protected_methods.map { |m| ['prot_method', m.to_s] } + "
                                        symbol-code)}))))

(deftest autocomplete-ruby-code
  (when (= :ruby @language)
    (with-plain-repl "completes ruby code" [repl eql]
      (testing "completes code with methods"
        (check (eql {:text/contents "pu" :text/range [[0 2] [0 2]] :text/language :ruby
                     :repl/evaluator repl}
                    [:completions/all])
               => {:completions/all (m/embeds [{:text/contents "puts"
                                                :completion/type :method/private}
                                               {:text/contents "public_methods"
                                                :completion/type :method/public}])}))
      #_
      (testing "getting local variables"
        (check (eql {:text/contents "fo" :text/range [[0 2] [0 2]] :text/language :ruby}
                    [:completions/code])
               => {:completions/code (str "binding.local_variables.map { |m| ['local_var', m.to_s] } + "
                                          "public_methods.map { |m| ['pub_method', m.to_s] } + "
                                          "private_methods.map { |m| ['priv_method', m.to_s] } + "
                                          "protected_methods.map { |m| ['prot_method', m.to_s] } + "
                                          symbol-code)})))))
