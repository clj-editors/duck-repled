(ns duck-repled.clojure.blocks-and-namespaces-test
  (:require [check.async :refer [check testing async-test]]
            [clojure.test :refer [deftest]]
            [duck-repled.core :as core]
            [promesa.core :as p]))

(def eql (core/gen-eql))

(deftest blocks
  (async-test "separates editor data into fragments" {:timeout 8000}
    (testing "gets block/selection/etc from `:text/*` elements"
      (check (eql {:text/contents "(ns lol)(+ 1 2)\n\n( \n ( 2 3))"
                   :text/language :clojure
                   :text/range [[3 3] [3 7]]}
                  [{:text/block [:text/contents :text/range]}
                   {:text/ns [:text/contents :text/range]}
                   {:text/top-block [:text/contents :text/range :repl/watch-point]}
                   {:text/current-var [:text/contents :text/range]}])
             => {:text/block {:text/range [[3 1] [3 6]]
                              :text/contents "( 2 3)"}
                 :text/top-block {:text/contents "( \n ( 2 3))"
                                  :repl/watch-point ""
                                  :text/range [[2 0] [3 7]]}
                 :text/current-var {:text/range [[3 3] [3 3]]
                                    :text/contents "2"}
                 :text/ns {:text/contents "lol"
                           :text/range [[0 0] [0 7]]}}))

    (testing "gets block/selection/etc from text editor"
      (check (eql {:editor/data {:contents "(ns lol)(+ 1 2)\n\n( \n ( 2 3))"
                                 :filename "file.cljc"
                                 :language :clojure
                                 :range [[3 3] [3 7]]}}
                  [{:editor/contents
                    [{:text/block [:text/contents :text/range :repl/namespace :file/filename]}
                     {:text/ns [:text/contents :text/range :file/filename]}
                     {:text/top-block [:text/contents :text/range :repl/namespace :file/filename]}
                     {:text/current-var [:text/contents :text/range :repl/namespace :file/filename]}]}])
             => {:editor/contents
                 {:text/block {:text/range [[3 1] [3 6]]
                               :text/contents "( 2 3)"
                               :repl/namespace 'lol
                               :file/filename "file.cljc"}
                  :text/top-block {:text/contents "( \n ( 2 3))"
                                   :text/range [[2 0] [3 7]]
                                   :repl/namespace 'lol
                                   :file/filename "file.cljc"}
                  :text/current-var {:text/range [[3 3] [3 3]]
                                     :text/contents "2"
                                     :repl/namespace 'lol
                                     :file/filename "file.cljc"}
                  :text/ns {:text/contents "lol"
                            :text/range [[0 0] [0 7]]
                            :file/filename "file.cljc"}}}))

    (testing "keep reference of the current namespace for each element"
      (check (eql {:editor/data {:contents "(ns lol)(+ 1 2)\n\n( \n ( 2 3))"
                                 :filename nil
                                 :language :clojure
                                 :range [[3 3] [3 7]]}}
                  [{:editor/contents
                    [{:text/block [:text/ns]}
                     {:text/top-block [:text/ns]}
                     {:text/current-var [:text/ns]}
                     {:text/selection [:text/ns]}]}])
             => {:editor/contents
                 {:text/block {:text/ns {:text/contents "lol"
                                         :text/range [[0 0] [0 7]]}}
                  :text/top-block {:text/ns {:text/contents "lol"
                                             :text/range [[0 0] [0 7]]}}
                  :text/current-var {:text/ns {:text/contents "lol"
                                               :text/range [[0 0] [0 7]]}}
                  :text/selection {:text/ns {:text/contents "lol"
                                             :text/range [[0 0] [0 7]]}}}}))))

(deftest config-for-repl
  (async-test "separates editor data into fragments" {:timeout 8000}
    (testing "configuring everything to be CLJ or CLJS"
      (check (eql {:config/repl-kind :clj, :config/eval-as :clj}
                  [:repl/kind])
             => {:repl/kind :clj})

      (check (eql {:config/repl-kind :clj, :config/eval-as :cljs} [:repl/kind])
             => {:repl/kind :cljs}))

    (testing "will always be 'another kind' if we're not in CLJ REPL"
      (check (eql {:config/repl-kind :cljs, :config/eval-as :clj}
                  [:config/repl-kind :repl/kind])
             => {:repl/kind :cljs}))

    (testing "if `:prefer-clj` is used, will use clj on .clj and .cljc files"
      (check (eql {:config/repl-kind :clj
                   :config/eval-as :prefer-clj
                   :file/filename "somefile.clj"}
                  [:repl/kind])
             => {:repl/kind :clj})

      (check (eql {:config/repl-kind :clj
                   :config/eval-as :prefer-clj
                   :file/filename "somefile.cljc"}
                  [:repl/kind])
             => {:repl/kind :clj})

      (check (eql {:config/repl-kind :clj
                   :config/eval-as :prefer-clj
                   :file/filename "somefile.cljs"}
                  [:repl/kind])
             => {:repl/kind :cljs}))

    (testing "if `:prefer-cljs` is used, will use cljs on .cljs and .cljc files"
      (check (eql {:config/repl-kind :clj
                   :config/eval-as :prefer-cljs
                   :file/filename "somefile.clj"}
                  [:repl/kind])
             => {:repl/kind :clj})

      (check (eql {:config/repl-kind :clj
                   :config/eval-as :prefer-cljs
                   :file/filename "somefile.cljc"}
                  [:repl/kind])
             => {:repl/kind :cljs})

      (check (eql {:config/repl-kind :clj
                   :config/eval-as :prefer-cljs
                   :file/filename "somefile.cljs"}
                  [:repl/kind])
             => {:repl/kind :cljs}))))

;; FIXME - these all need to pass Clojure lang
(deftest ns-from-contents
  (async-test "separates editor data into fragments" {:timeout 8000}
    (p/let [seed {:text/language :clojure
                  :editor/contents
                  {:text/contents
                   "\n(ns first.namespace)\n\n(+ 1 2)\n\n(ns second.ns)\n\n(+ 3 4)"}}]
      (testing "gets namespace if declaration is below current selection"
        (check (eql (assoc-in seed [:editor/contents :text/range] [[0 0] [0 0]])
                    [{:editor/contents [:text/ns]}])
               => {:editor/contents
                   {:text/ns {:text/range [[1 0] [1 19]]
                              :text/contents "first.namespace"}}}))

      (testing "gets namespace if declaration is above current selection"
        (check (eql (assoc-in seed [:editor/contents :text/range] [[2 0] [2 0]])
                    [{:editor/contents [:text/ns]}])
               => {:editor/contents
                   {:text/ns {:text/range [[1 0] [1 19]]
                              :text/contents "first.namespace"}}})

        (check (eql (assoc-in seed [:editor/contents :text/range]  [[5 0] [5 0]])
                    [{:editor/contents [:text/ns]}])
               => {:editor/contents
                   {:text/ns {:text/range [[5 0] [5 13]]
                              :text/contents "second.ns"}}}))

      (testing "gets REPL namespace if a ns exists"
        (check (eql (assoc-in seed [:editor/contents :text/range]  [[2 0] [2 0]])
                    [{:editor/contents [:text/ns :repl/namespace :text/language]}])
               => {:editor/contents
                   {:repl/namespace 'first.namespace}}))

      (testing "fallback to default if there's no NS in editor"
        (check (eql {:editor/contents {:text/contents "" :text/range [[2 0] [2 0]]}
                     :text/language :clojure
                     :repl/kind :clj}
                    [{:editor/contents [:repl/namespace]}])
               => {:editor/contents
                   {:repl/namespace 'user}})

        (check (eql {:editor/contents {:text/contents "" :text/range [[2 0] [2 0]]}
                     :text/language :clojure
                     :repl/kind :cljs}
                    [{:editor/contents [:repl/namespace]}])
               => {:editor/contents
                   {:repl/namespace 'cljs.user}})))))

(deftest read-file-contents
  (async-test "read file contents and extract fragments"
    (check (eql {:file/filename "test/duck_repled/tests.cljs"
                 :text/language :clojure}
                [{:file/contents [:text/ns]}])
           => {:file/contents {:text/ns {:text/contents "duck-repled.tests"}}})

    (check (eql {:file/filename "test/duck_repled/tests.cljs"
                 :text/language :clojure}
                [{'(:file/contents {:patch {:text/range [[33 0] [33 0]]}})
                  [:text/contents :text/range :text/top-blocks :text/top-block]}])
           => {:file/contents {:text/top-block
                               {:text/contents #"^.defn main"
                                :text/range [[29 0] [59 5]]}}})))
