(ns duck-repled.autocomplete-test
  (:require [clojure.test :refer [deftest]]
            [duck-repled.core :as duck]
            [orbit.evaluation :as eval]
            [promesa.core :as p]
            [duck-repled.core :as core]
            [duck-repled.repl-helpers :as helpers]
            [check.async :refer [testing check async-test]]
            [matcher-combinators.matchers :as m]))

(deftest autocompletes-vars
  (async-test "autocompletion of vars"
    (testing "defined in current NS"
      (helpers/with-orbit-repl! [repl]
        (p/let [eql (core/gen-eql)]
          (check (eql {:text/contents "my-" :repl/evaluator repl
                       :repl/kind :clj :repl/namespace 'foo}
                      [:completions/var])
                 => {:completions/var (m/embeds [{:text/contents "my-fun"}])}))))

    (testing "defined in core/referred"
      (helpers/with-orbit-repl! [repl]
        (p/let [eql (core/gen-eql)]
          (check (eql {:text/contents "re-" :repl/evaluator repl
                       :repl/kind :clj :repl/namespace 'foo}
                      [:completions/var])
                 => {:completions/var (m/embeds [{:text/contents "re-find"}
                                                 {:text/contents "re-matches"}
                                                 {:text/contents "re-pattern"}
                                                 {:text/contents "re-seq"}])}))))

    (testing "imported"
      (helpers/with-orbit-repl! [repl]
        (p/let [eql (core/gen-eql)]
          (check (eql {:text/contents "str/re" :repl/evaluator repl
                       :repl/kind :clj :repl/namespace 'foo}
                      [:completions/var])
                 => {:completions/var (m/embeds [{:text/contents "str/replace"}
                                                 {:text/contents "str/replace-first"}])}))))))

(deftest autocompletes-keywords
  (async-test "autocompletion of keywords"
    (helpers/with-orbit-excluding-kinds [repl] #{:bb :org.babashka/nbb}
      (testing "gets keywords from the core namespace"
        (p/let [eql (core/gen-eql)]
          (check (eql {:text/contents ":gen" :repl/evaluator repl
                       :repl/kind :clj :repl/namespace 'foo}
                      [:completions/keyword])
                 => {:completions/keyword (m/embeds [{:text/contents ":gen-class"}])})))

      (testing "gets keywords from other namespaces"
        (p/let [eql (core/gen-eql)]
          (check (eql {:text/contents ":clojure.error/k" :repl/evaluator repl
                       :repl/kind :clj :repl/namespace 'foo}
                      [:completions/keyword])
                 => {:completions/keyword (m/embeds [{:text/contents ":clojure.error/keys"}])})))

      (testing "resolves keywords from other namespaces"
        (p/let [eql (core/gen-eql)]
          (check (eql {:text/contents "::str/sat" :repl/evaluator repl :repl/namespace 'foo
                       :repl/kind :clj}
                      [:completions/keyword])
                 => {:completions/keyword (m/embeds [{:text/contents "::str/satisfies?"}])}))))))

(deftest autocompletes-cljs-code
  (when @helpers/cljs?
    (async-test "autocompletion of vars"
      (helpers/with-orbit-repl! [repl]
        (p/let [eql (core/gen-eql)
                seed {:repl/evaluator repl
                      :editor/data {:contents "(ns foo)\nflavor"
                                    :range [[1 0] [1 0]]
                                    :language :clojure
                                    :filename "file.cljs"}
                      :config/eval-as :prefer-clj}]
          (testing "defined in current NS"
            (check (eql (assoc seed :text/contents "my-" :repl/namespace 'foo)
                        [:completions/var])
                   => {:completions/var
                       (m/embeds [{:completion/type :function :text/contents "my-fun"}])}))

          (testing "defined in core"
            (check (eql (assoc seed :text/contents "re-" :repl/namespace 'foo)
                        [:completions/var])
                   => {:completions/var (m/embeds [{:text/contents "re-find"}
                                                   {:text/contents "re-matches"}
                                                   {:text/contents "re-pattern"}
                                                   {:text/contents "re-seq"}])}))

          (testing "from imported namespaces"
            (check (eql (assoc seed :text/contents "str/replace-" :repl/namespace 'foo)
                        [:completions/var])
                   => {:completions/var (m/embeds [{:text/contents "str/replace-with"}
                                                   {:text/contents "str/replace-first"}])})))))))

(deftest autocompletes-cljs-keywords
  (when @helpers/cljs?
    (async-test "autocompletion of keywords"
      (helpers/with-orbit-repl! [repl]
        (p/let [eql (core/gen-eql)
                seed {:repl/evaluator repl
                      :editor/data {:contents "(ns foo)\nflavor"
                                    :range [[1 0] [1 0]]
                                    :language :clojure
                                    :filename "file.cljs"}
                      :config/eval-as :prefer-clj}]
          (testing "unnamespaced"
            (check (eql (assoc seed :text/contents ":arg" :repl/namespace 'foo)
                        [:completions/keyword])
                   => {:completions/keyword
                       (m/embeds [{:completion/type :keyword :text/contents ":arglists"}])}))

          (testing "fully namespaced"
            (check (eql (assoc seed :text/contents ":clojure.string/sat" :repl/namespace 'foo)
                        [:completions/keyword])
                   => {:completions/keyword
                       (m/embeds [{:text/contents ":clojure.string/satisfies?"}])}))

          (testing "alias namespaced"
            (check (eql (assoc seed :text/contents "::str/sat" :repl/namespace 'foo)
                        [:completions/keyword])
                   => {:completions/keyword
                       (m/embeds [{:text/contents "::str/satisfies?"}])}))

          (testing "default alias namespaced"
            (check (eql (assoc seed :text/contents "::full" :repl/namespace 'foo)
                        [:completions/keyword])
                   => {:completions/keyword
                       (m/embeds [{:text/contents "::fully-qualified"}])})))))))

(deftest nrepl-autocomplete
  (async-test "uses default nREPL completion to autocomplete"
    (helpers/with-orbit-repl! [repl]
      (p/let [eql (core/gen-eql)
              seed {:repl/evaluator repl
                    :editor/data {:contents "\n(let [foobar 10]\n  (foa))"
                                  :range [[2 5] [2 5]]
                                  :language :clojure
                                  :filename "file.clj"}
                    :config/eval-as :prefer-clj}
              completions (eval/evaluate repl
                                         {:prefix "fa"}
                                         {:options {:kind :clj/aux :op "completions"}})]
        (when (:result completions)
          (check (eql seed [{:editor/contents [:completions/nrepl]}])
                 => {:editor/contents
                     {:completions/nrepl
                      (m/embeds [{:completion/type :macro :text/contents "for"}
                                 {:completion/type :function :text/contents "force"}
                                 {:completion/type :function :text/contents "format"}])}}))))))

(deftest compliment-autocomplete
  (async-test "uses compliment to autocomplete"
    (helpers/with-orbit-repl! [repl]
      (p/let [compliment? (eval/evaluate repl "(require 'compliment.core)"
                                         {:options {:kind :clj/aux}})
              eql (core/gen-eql)
              seed {:repl/evaluator repl
                    :editor/data {:contents "\n(let [foobar 10]\n  (foa))"
                                  :range [[2 5] [2 5]]
                                  :language :clojure
                                  :filename "file.clj"}
                    :config/eval-as :prefer-clj}]
        (when (-> compliment? (:result :not-found) nil?)
          (check (eql seed [{:editor/contents [:completions/external]}])
                 => {:editor/contents
                     {:completions/external
                      (m/embeds [{:completion/type :macro :text/contents "for"}
                                 {:completion/type :function :text/contents "force"}
                                 {:completion/type :function :text/contents "format"}
                                 {:completion/type :local :text/contents "foobar"}])}}))))))

(deftest suitable-autocomplete
  (async-test "uses compliment to autocomplete"
    (helpers/with-orbit-repl! [repl]
      (p/let [compliment? (eval/evaluate repl "(require 'compliment.core)"
                                         {:options {:kind :clj/aux}})
              _ (eval/evaluate repl "(require 'suitable.compliment.sources.cljs)"
                               {:options {:kind :clj/aux}})
              suitable? (eval/evaluate repl "(require 'suitable.js-completions)"
                                       {:options {:kind :clj/aux}})
              eql (core/gen-eql)
              seed {:repl/evaluator repl
                    :editor/data {:contents "\n(let [foobar 10]\n  (foa))"
                                  :language :clojure
                                  :range [[2 5] [2 5]]
                                  :filename "file.cljs"}
                    :config/eval-as :prefer-clj}]

        (when (-> suitable? (:result :not-found) nil?)
          (p/do!
           (check (eql seed [{:editor/contents [:text/top-block :text/range :completions/external]}])
                  => {:editor/contents
                      {:completions/external
                       (m/embeds [{:completion/type :macro :text/contents "for"}
                                  {:completion/type :function :text/contents "force"}
                                  {:completion/type :local :text/contents "foobar"}])}})

           (check (eql (update seed :editor/data merge {:contents "js/pr"
                                                        :language :clojure
                                                        :range [[0 5] [0 5]]})
                       [{:editor/contents [:completions/external]}])
                  => {:editor/contents
                      {:completions/external
                       (m/embeds [{:completion/type :var :text/contents "js/process"}
                                  {:completion/type :function :text/contents "js/propertyIsEnumerable"}])}})))))))
