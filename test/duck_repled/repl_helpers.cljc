(ns duck-repled.repl-helpers
  #?(:cljs (:require-macros [duck-repled.repl-helpers]))
  (:require #?(:cljs [orbit.shadow.evaluator :as shadow])
            [orbit.evaluation :as eval]
            [promesa.core :as p]
            [clojure.string :as str]
            [clojure.edn :as edn]
            [sci.core :as sci]
            #?(:cljs ["net" :as net])
            #?(:cljs ["fs" :as fs])))

(defn- treat-data [pending data]
  (let [full (:buffer (swap! pending update :buffer str data))
        [_ id result] (re-find #"(?s)__eval_result_beg\s+(.*?)\s+(.*?)\s+__eval_result_end" full)]
    ; (prn :FRAG full)
    ; (prn :RESULT id result)
    (when result
      (let [{:keys [id promise opts]} (:pending @pending)
            result (try
                      (edn/read-string {:default tagged-literal} result)
                      (catch #?(:cljs :default :clj Throwable) _
                        {(if (re-find #"^\{:result" result) :result :error)
                         result}))]
        (swap! pending dissoc :buffer :pending)
        (when promise
          (p/resolve! promise (assoc result :options opts)))))))

(defn- connect! [host port]
  #?(:clj nil
     :cljs
     (js/Promise.
      (fn [resolve fail]
        (let [pending (atom {})
              conn (. net createConnection port host)]
          (.on conn "connect" #(resolve {:conn conn :pending pending}))
          (.on conn "data" #(treat-data pending %)))))))

(defonce host (atom "localhost"))
(defonce nrepl-port
  (atom (try #?(:clj (-> ".shadow-cljs/nrepl.port" slurp Integer/parseInt)
                :cljs (-> ".shadow-cljs/nrepl.port" (fs/readFileSync "UTF-8") js/parseInt))
          (catch #?(:clj Throwable :cljs :default) _
            nil))))
(defonce language (atom nil))

;; FIXME - fix when Clojure implementations are made
(defonce connect-fn (atom #?(:clj nil :cljs shadow/connect!)))
(defonce cljs? (delay #?(:clj nil :cljs (= @connect-fn shadow/connect!))))

(defn prepare-orbit-repl! []
  (p/let [repl# (@connect-fn @host @nrepl-port identity)]
    (when @cljs? (p/delay 200))
    repl#))

#?(:clj (defmacro with-orbit-repl! [binding & body]
          `(p/let ~(conj binding `(prepare-orbit-repl!))
             (p/all
              (for [[file# kind#] [["file.clj" :clj/eval] ["file.cljs" :cljs/eval]]]
                (p/do!
                 (eval/evaluate ~(first binding) "(ns foo (:require [clojure.string :as str]))\n"
                                {:filename file# :options {:kind kind#}})
                 (eval/evaluate ~(first binding)
                                "\n(defn my-fun \"My doc\" [] (+ 1 2))\n"
                                {:filename file# :namespace ~''foo :options {:kind kind#}})
                 (eval/evaluate ~(first binding)
                                "(def some-var 10)\n"
                                {:filename file# :namespace ~''foo :options {:kind kind#}})
                 (eval/evaluate ~(first binding)
                                "(def keyword-example1 :clojure.string/satisfies?)\n"
                                {:filename file# :namespace ~''foo :options {:kind kind#}})
                 (eval/evaluate ~(first binding)
                                "(def keyword-example2 ::fully-qualified)\n"
                                {:filename file# :namespace ~''foo :options {:kind kind#}}))))
             (p/finally
              (p/do! ~@body)
              #(eval/close! ~(first binding))))))

#?(:clj (defmacro with-orbit-excluding-kinds [binding kinds & body]
          (let [command (str "#?("
                             (str/join " " (mapcat vector kinds (repeat false)))
                             " :default true)")]
            `(helpers/with-orbit-repl! ~binding
               (p/then
                (eval/evaluate ~(first binding) ~command)
                (fn [res#]
                  (when res#
                    (p/do! ~@body))))))))

#?(:clj (defmacro with-orbit-including-kinds [binding kinds & body]
          (let [command (str "#?("
                             (when (kinds :clj) ":bb false ")
                             (str/join " " (mapcat vector kinds (repeat true)))
                             " :default false)")]
            `(helpers/with-orbit-repl! ~binding
               (p/then
                (eval/evaluate ~(first binding) ~command {:plain true
                                                          :options {:kind :clj/aux}})
                (fn [res#]
                  (when res#
                    (p/do! ~@body))))))))

#?(:clj (defmacro with-eql [description binding & body]
          (let [binding (conj binding `(duck-repled.core/gen-eql))]
            `(let ~binding
               (check.async/async-test ~description
                 ~@body)))))

#?(:clj (defmacro with-plain-repl [description [repl eql] & body]
          `(p/let [~repl (prepare-orbit-repl!)
                   ~eql (duck-repled.core/gen-eql)]
             (p/finally (p/do! ~@body) #(eval/close! ~repl)))))
