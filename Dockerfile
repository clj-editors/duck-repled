FROM openjdk:19-jdk-alpine

USER root
RUN apk add --no-cache curl npm bash git
RUN wget https://raw.githubusercontent.com/technomancy/leiningen/stable/bin/lein
RUN chmod +x lein
RUN mv lein /usr/local/bin

WORKDIR /work
COPY . .

RUN npm install
RUN lein shadow-release
